double PVFromT_P_U_V_lat(double u000, double v000, double t000_K, double p000_hPa, double lon000_deg, double lat000_deg,
                         double u010, double t010_K, double p010_hPa, double lon010_deg, double lat010_deg, double q010,
                         double u110, double t110_K, double p110_hPa, double q110,
                         double u_110, double t_110_K, double p_110_hPa, double q_110,
                         double v001, double t001_K, double p001_hPa, double lon001_deg, double lat001_deg, double q001,
                         double v101, double t101_K, double p101_hPa, double q101,
                         double v_101, double t_101_K, double p_101_hPa, double q_101,
                         double t100_K, double p100_hPa)
{
    // Constants

    double radius_m = MetConstants::EARTH_RADIUS_km * 1000.;
    double PVU_factor = MetConstants::PVU_FACTOR;
    double g = MetConstants::GRAVITY_ACCELERATION;

    // Compute potential temperature on required grid points.

    double theta000_K = potTemp(t000_K, p000_hPa);

    double theta001_K = potTemp(t001_K, p001_hPa);
    double theta101_K = potTemp(t101_K, p101_hPa);
    double theta_101_K = potTemp(t_101_K, p_101_hPa);

    double theta010_K = potTemp(t010_K, p010_hPa);
    double theta110_K = potTemp(t110_K, p110_hPa);
    double theta_110_K = potTemp(t_110_K, p_110_hPa);

    // Compute isentropic relative vorticity:
    // (dv/dx)_theta - (du/dy)_theta

        // ... v = v000 + dv_theta (change on theta surface)

        double v;
        bool xslope_positive = false;

        if (theta001_K - theta000_K < 0)
        {
            xslope_positive = true;
            v = v001 + (v101 - v001) * abs((theta001_K - theta000_K) / (theta001_K - theta101_K));
        }
        else
        {
            v = v001 + (v_101 - v001) * abs((theta001_K - theta000_K) / (theta001_K - theta_101_K));
        }

        double dv_theta = v - v000;

        // ... Compute distance via pythagorean theorem:
        // ... dx_theta = { dx^2 + [ (v - v001)/(v101 - v001) * dz ]^2 }^0.5

            // ... ... dx
            double dx = gcDistance_deg(lon000_deg, lat000_deg,
                                       lon001_deg, lat001_deg,
                                       radius_m);

            // ... ... Convert pressure to height difference:
            // ... ... dz = R * T_v * d(lnp), d(lnp) = ln(p001 / p101)

            double T_v_mean_vx = 0.;
            double dPhi_vx = 0.;
            double dz_vx = 0.;
            double dx_theta = 0;

            if (xslope_positive)
            {
                T_v_mean_vx = (virtualTempFromSpecHum(t101_K, q101) + virtualTempFromSpecHum(t001_K, q001)) / 2.;
                dPhi_vx = geopotThickness(T_v_mean_vx, p101_hPa, p001_hPa);
                dz_vx = geopotToZ(dPhi_vx);
                dx_theta = sqrt(pow(dx, 2) + pow((v - v001) / (v101 - v001) * dz_vx, 2));
            }
            else
            {
                T_v_mean_vx = (virtualTempFromSpecHum(t_101_K, q_101) + virtualTempFromSpecHum(t001_K, q001)) / 2.;
                dPhi_vx = geopotThickness(T_v_mean_vx, p001_hPa, p_101_hPa);
                dz_vx = geopotToZ(dPhi_vx);
                dx_theta = sqrt(pow(dx, 2) + pow((v - v001) / (v_101 - v001) * dz_vx, 2));
            }


        // ... u = u000 + du_theta (change on theta surface)

        double u;
        bool yslope_positive = false;

        if (theta010_K - theta000_K < 0)
        {
            yslope_positive = true;
            u = u010 + (u110 - u010) * abs((theta010_K - theta000_K) / (theta010_K - theta110_K));
        }
        else
        {
            u = u010 + (u_110 - u010) * abs((theta010_K - theta000_K) / (theta010_K - theta_110_K));
        }

        double du_theta = u - u000;

        // ... Compute distance via pythagorean theorem:
        // ... dy_theta = { dy^2 + [ (u - u010)/(u110 - u010) * dz ]^2 }^0.5

            // ... ... dy
            double dy = gcDistance_deg(lon000_deg, lat000_deg,
                                       lon010_deg, lat010_deg,
                                       radius_m);

            // ... ... Convert pressure to height difference:
            // ... ... dz = R * T_v * d(lnp), d(lnp) = ln(p001 / p101

            double T_v_mean_uy = 0.;
            double dPhi_uy = 0.;
            double dz_uy = 0.;
            double dy_theta = 0.;

            if(yslope_positive)
            {
                T_v_mean_uy = (virtualTempFromSpecHum(t110_K, q110) + virtualTempFromSpecHum(t010_K, q010)) /2.;
                dPhi_uy = geopotThickness(T_v_mean_uy, p110_hPa, p010_hPa);
                dz_uy = geopotToZ(dPhi_uy);
                dy_theta = sqrt(pow(dy, 2) + pow((u - u010) / (u110 - u010) * dz_uy, 2));
            }
            else
            {
                T_v_mean_uy = (virtualTempFromSpecHum(t_110_K, q_110) + virtualTempFromSpecHum(t010_K, q010)) /2.;
                dPhi_uy = geopotThickness(T_v_mean_uy, p010_hPa, p_110_hPa);
                dz_uy = geopotToZ(dPhi_uy);
                dy_theta = sqrt(pow(dy, 2) + pow((u - u010) / (u_110 - u010) * dz_uy, 2));
            }


    // (dv/dx)_theta - (du/dy)_theta
    double relVo = (dv_theta / dx_theta) - (du_theta / dy_theta);


    // Compute planetary vorticity f = 2 * w_0 * sin(2 * PI * lat / 360).
    double planVo = planetaryVorticity(lat000_deg);

    // Compute vertical theta gradient.

    double dTheta_K = potTemp(t100_K, p100_hPa) - potTemp(t000_K, p000_hPa);
    double dp_Pa = 100. * (p100_hPa - p000_hPa);

    // Compute and return PV.

    double PV = -g * PVU_factor * (planVo + relVo) * (dTheta_K / dp_Pa);
    return PV;
}

double potTemp(double temp_K,
               double pressure_hPa)
{
    // Constants

    double R = MetConstants::GAS_CONSTANT_DRY_AIR;
    double c_p = MetConstants::SPEC_HEAT_DRY_PRESSURE;

    double potTemp_K = temp_K * pow(1000. / pressure_hPa, R / c_p);
    return potTemp_K;
}
double virtualTempFromSpecHum(double temp_K,
                              double specHum)
{
    double virtualTemp_K = temp_K * (1 + specHum * 0.608);

    return virtualTemp_K;
}

double geopotThickness(double meanVirtualTemp_K,
                       double p_top, double p_bot)
{
    // Constants
    double R = MetConstants::GAS_CONSTANT_DRY_AIR;

    double dln_p = log(p_bot / p_top);
    double dPhi = R * meanVirtualTemp_K * dln_p;

    return dPhi;
}

double geopotToZ(double geopotential)
{
    // Constants
    double g = MetConstants::GRAVITY_ACCELERATION;

    double Z = geopotential / g;

    return Z;
}

double planetaryVorticity(double lat_deg)
{
    // Constants
    double w_0 = MetConstants::EARTH_ROTATION_RATE;

    double f = 2. * w_0 * sin(2. * M_PI * lat_deg / 360.);

    return f;
}


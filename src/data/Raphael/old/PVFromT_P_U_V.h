/*
 * Computes isentropic potential vorticity [PVU] from
 * horizontal wind components [m/s],
 * temperature [K],
 * pressure [hPa],
 * lon & lat [deg]
 * and specific humidity [dimensionless].
 * THIS METHOD IS POSSIBLY INACCURATE!
 */
double PVFromT_P_U_V_lat(double u000, double v000, double t000_K, double p000_hPa, double lon000_deg, double lat000_deg,
                         double u010, double t010_K, double p010_hPa, double lon010_deg, double lat010_deg, double q010,
                         double u110, double t110_K, double p110_hPa, double q110,
                         double u_110, double t_110_K, double p_110_hPa, double q_110,
                         double v001, double t001_K, double p001_hPa, double lon001_deg, double lat001_deg, double q001,
                         double v101, double t101_K, double p101_hPa, double q101,
                         double v_101, double t_101_K, double p_101_hPa, double q_101,
                         double t100_K, double p100_hPa);

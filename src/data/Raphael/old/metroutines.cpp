/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015 Marc Rautenhaus
**  Copyright 2015 Michael Kern
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "metroutines.h"

// standard library imports
#include <iostream>
#include <cmath>

// related third party imports

// local application imports

using namespace std;


namespace Met3D
{

inline double degreesToRadians(double angle)
{
    return angle / 180. * M_PI;
}


inline double haversin(double radians)
{
    double sinValue = sin(radians / 2.);
    return sinValue * sinValue;
}


double gcDistanceUnitSphere_2(
        const double lon1_rad, const double lat1_rad,
        const double lon2_rad, const double lat2_rad)
{
    const double deltaLon = lon2_rad - lon1_rad;
    const double deltaLat = lat2_rad - lat1_rad;

    // Compute distance using the haversine formula.
    const double havSinAlpha =
            haversin(deltaLat) + std::cos(lat1_rad) * std::cos(lat2_rad)
            * haversin(deltaLon);

    return 2. * std::asin(std::sqrt(havSinAlpha));
}


double gcDistanceUnitSphere(
        const double lon1_rad, const double lat1_rad,
        const double lon2_rad, const double lat2_rad)
{
    double dlon = lon2_rad - lon1_rad;
    double dlat = lat2_rad - lat1_rad;

    double sin_dlat = sin(dlat/2.);
    double sin_dlon = sin(dlon/2.);
    double a = sin_dlat*sin_dlat
            + cos(lat1_rad) * cos(lat2_rad) * sin_dlon*sin_dlon;
    double c = 2. * asin(min(1.,sqrt(a)));
    return c;
}


double gcDistance(const double lon1_rad, const double lat1_rad,
                  const double lon2_rad, const double lat2_rad,
                  const double radius)
{
    return gcDistanceUnitSphere(
                lon1_rad, lat1_rad, lon2_rad, lat2_rad) * radius;
}


double gcDistance_deg(const double lon1, const double lat1,
                      const double lon2, const double lat2,
                      const double radius)
{
    return gcDistanceUnitSphere(
                degreesToRadians(lon1), degreesToRadians(lat1),
                degreesToRadians(lon2), degreesToRadians(lat2)) * radius;
}


inline double cot(double radians)
{
    return 1. / std::tan(radians);
}


double gcTriangleArea(const double lon1, const double lat1,
                      const double lon2, const double lat2,
                      const double lon3, const double lat3,
                      const double radius)
{
    using namespace std;

    // Great circle distances between the vertices.
    const double c = gcDistanceUnitSphere(
                degreesToRadians(lon1), degreesToRadians(lat1),
                degreesToRadians(lon2), degreesToRadians(lat2));

    const double a = gcDistanceUnitSphere(
                degreesToRadians(lon2), degreesToRadians(lat2),
                degreesToRadians(lon3), degreesToRadians(lat3));

    const double b = gcDistanceUnitSphere(
                degreesToRadians(lon1), degreesToRadians(lat1),
                degreesToRadians(lon3), degreesToRadians(lat3));

    const double PI_HALF = M_PI / 2.0f;

    // Angles between the triangle sides in radians.
    double A,B,C;

    // Sherical excess.
    double E = 0;

    // Handle quadrantal triangle case by applying napier's rule.
    const double PRECISION = 0.000001;

    // If all sides are PI_HALF...
    if (abs(a - PI_HALF) <= PRECISION &&
            abs(b - PI_HALF) <= PRECISION &&
            abs(c - PI_HALF) <= PRECISION)
    {
        // ...all three angles are M_PI / 2.
        E = PI_HALF;
    }

    // If two sides are PI_HALF the corresponding two angles are PI_HALF,
    // the third angle = opposite side.
    else if (abs(a - PI_HALF) <= PRECISION &&
             abs(b - PI_HALF) <= PRECISION)
    {
        E = c;
    }

    else if (abs(b - PI_HALF) <= PRECISION &&
             abs(c - PI_HALF) <= PRECISION)
    {
        E = a;
    }

    else if (abs(a - PI_HALF) <= PRECISION &&
             abs(c - PI_HALF) <= PRECISION)
    {
        E = b;
    }

    // If one side is PI_HALF apply Napier's rule.
    else if (abs(a - PI_HALF) <= PRECISION)
    {
        A = acos(-cot(c) * cot(b) );
        B = asin( sin(A) * sin(b) );
        C = asin( sin(A) * sin(c) );

        E = A + B + C - M_PI;
    }
    else if (abs(b - PI_HALF) <= PRECISION)
    {
        B = acos(-cot(c) * cot(a) );
        A = asin( sin(B) * sin(a) );
        C = asin( sin(B) * sin(c) );

        E = A + B + C - M_PI;
    }
    else if (abs(c - PI_HALF) <= PRECISION)
    {
        C = acos(-cot(a) * cot(b) );
        A = asin( sin(C) * sin(a) );
        B = asin( sin(C) * sin(b) );

        E = A + B + C - M_PI;
    }

    // ...else apply spherical trigonometry.
    else
    {
        const double cosa = cos(a);
        const double cosb = cos(b);
        const double cosc = cos(c);

        const double sina = sin(a);
        const double sinb = sin(b);
        const double sinc = sin(c);

        A = acos( (cosa - cosb * cosc) / (sinb * sinc) );
        B = acos( (cosb - cosc * cosa) / (sinc * sina) );
        C = acos( (cosc - cosa * cosb) / (sina * sinb) );

        E = A + B + C - M_PI;
    }

    return radius * radius * E;
}


double gcQuadrilateralArea(const double lon1, const double lat1,
                           const double lon2, const double lat2,
                           const double lon3, const double lat3,
                           const double lon4, const double lat4,
                           const double radius)
{
    return gcTriangleArea(lon1, lat1, lon2, lat2, lon3, lat3, radius) +
            gcTriangleArea(lon3, lat3, lon2, lat2, lon4, lat4, radius);
}


double pressure2metre_standardICAO(double p_Pa)
{
    // g and R are used by all equations below.
    double g = MetConstants::GRAVITY_ACCELERATION;
    double R = MetConstants::GAS_CONSTANT_DRY_AIR;

    double z = 0.;
    if (p_Pa < 1011.)
    {
        // Pressure to metre conversion not implemented for z > 32km
        // (p ~ 10.11 hPa).
        return M_MISSING_VALUE;
    }

    else if (p_Pa < 5475.006582501095)
    {
        // ICAO standard atmosphere between 20 and 32 km: T(z=20km) = -56.5
        // degC, p(z=20km) = 54.75 hPa. Temperature gradient is -1.0 K/km.
        double z0 = 20000.;
        double T0 = 216.65;
        double gamma = -1.0e-3;
        double p0 = 5475.006582501095;

        // Hydrostatic equation with linear temperature gradient.
        z = 1./gamma * (T0 - (T0-gamma*z0) * exp(gamma*R/g * log(p_Pa/p0)));
    }

    else if (p_Pa < 22632.)
    {
        // ICAO standard atmosphere between 11 and 20 km: T(z=11km) = -56.5
        // degC, p(z=11km) = 226.32 hPa. Temperature is constant at -56.5 degC.
        double z0 = 11000.;
        double p0 = 22632.;
        double T = 216.65;

        // Hydrostatic equation with constant temperature profile.
        z = z0 - (R*T)/g * log(p_Pa/p0);
    }

    else
    {
        // ICAO standard atmosphere between 0 and 11 km: T(z=0km) = 15 degC,
        // p(z=0km) = 1013.25 hPa. Temperature gradient is 6.5 K/km.
        double z0 = 0.;
        double T0 = 288.15;
        double gamma = 6.5e-3;
        double p0 = 101325.;

        // Hydrostatic equation with linear temperature gradient.
        z = 1./gamma * (T0 - (T0-gamma*z0) * exp(gamma*R/g * log(p_Pa/p0)));
    }

    return z;
}


double metre2pressure_standardICAO(double z_m)
{
    // g and R are used by all equations below.
    double g = MetConstants::GRAVITY_ACCELERATION;
    double R = MetConstants::GAS_CONSTANT_DRY_AIR;

    if (z_m <= 11000.)
    {
        // ICAO standard atmosphere between 0 and 11 km: T(z=0km) = 15 degC,
        // p(z=0km) = 1013.25 hPa. Temperature gradient is 6.5 K/km.
        double z0 = 0.;
        double T0 = 288.15;
        double gamma = 6.5e-3;
        double p0 = 101325.;

        // Hydrostatic equation with linear temperature gradient.
        double p = p0 * pow((T0-gamma*z_m) / (T0-gamma*z0), g/(gamma*R));
        return p;
    }

    else if (z_m <= 20000.)
    {
        // ICAO standard atmosphere between 11 and 20 km: T(z=11km) = -56.5
        // degC, p(z=11km) = 226.32 hPa. Temperature is constant at -56.5 degC.
        double z0 = 11000.;
        double p0 = 22632.;
        double T = 216.65;

        // Hydrostatic equation with constant temperature profile.
        double p = p0 * exp(-g * (z_m-z0) / (R*T));
        return p;
    }

    else if (z_m <= 32000.)
    {
        // ICAO standard atmosphere between 20 and 32 km: T(z=20km) = -56.5
        // degC, p(z=20km) = 54.75 hPa. Temperature gradient is -1.0 K/km.
        double z0 = 20000.;
        double T0 = 216.65;
        double gamma = -1.0e-3;
        double p0 = 5475.006582501095;

        // Hydrostatic equation with linear temperature gradient.
        double p = p0 * pow((T0-gamma*z_m) / (T0-gamma*z0), g/(gamma*R));
        return p;
    }

    else
    {
        // Metre to pressure conversion not implemented for z > 32km.
        return M_MISSING_VALUE;
    }
}


double isaTemperature(double z_m)
{
    if (z_m <= 11000.)
    {
        // ICAO standard atmosphere between 0 and 11 km: T(z=0km) = 15 degC,
        // p(z=0km) = 1013.25 hPa. Temperature gradient is 6.5 K/km.
        double T0 = 288.15;
        double gamma = 6.5e-3;
        return T0-gamma*z_m;
    }

    else if (z_m <= 20000.)
    {
        // ICAO standard atmosphere between 11 and 20 km: T(z=11km) = -56.5 degC,
        // p(z=11km) = 226.32 hPa. Temperature is constant at -56.5 degC.
        double T = 216.65;
        return T;
    }

    else if (z_m <= 32000.)
    {
        // ICAO standard atmosphere between 20 and 32 km: T(z=20km) = -56.5 degC,
        // p(z=20km) = 54.75 hPa. Temperature gradient is -1.0 K/km.
        double z0 = 20000.;
        double T0 = 216.65;
        double gamma = -1.0e-3;
        return T0-gamma*(z_m-z0);
    }

    else
    {
        // ISA temperature from flight level not implemented for z > 32km.
        return M_MISSING_VALUE;
    }
}


double flightlevel2metre(double flightlevel)
{
    // Convert flight level (ft) to m (1 ft = 30.48 cm; 1/0.3048m = 3.28...).
    return flightlevel * 100. / 3.28083989501;
}


double metre2flightlevel(double z_m)
{
    // Convert flight level (ft) to m (1 ft = 30.48 cm; 1/0.3048m = 3.28...).
    return z_m * 3.28083989501 / 100.;
}


double columnAirmass(double pbot_Pa, double ptop_Pa, double area_m2)
{
    // Gravity acceleration (m/s2).
    double g = MetConstants::GRAVITY_ACCELERATION;

    // m*g = dp*A --> m = dp/g * A
    double mass_kg = abs(pbot_Pa - ptop_Pa) / g * area_m2;
    return mass_kg;
}


double boxVolume_dry(double p_Pa, double mass_kg, double temp_K)
{
    // Gas constant for dry air.
    double R_dry = MetConstants::GAS_CONSTANT_DRY_AIR;

    // Ideal gas law, pV = mRT.
    double vol = mass_kg * R_dry * temp_K / p_Pa;
    return vol;
}


double boxVolume_dry(double northWestLon, double northWestLat,
                     double southEastLon, double southEastLat,
                     double pmid_Pa, double pbot_Pa, double ptop_Pa,
                     double temp_K)
{
    double area_km2 = gcQuadrilateralArea(northWestLon, southEastLat,
                                          southEastLon, southEastLat,
                                          northWestLon, northWestLat,
                                          southEastLon, northWestLat,
                                          MetConstants::EARTH_RADIUS_km);

    double area_m2 = area_km2 * 1.E6;
    double mass_kg = columnAirmass(pbot_Pa, ptop_Pa, area_m2);

    if (temp_K == M_MISSING_VALUE)
        temp_K  = isaTemperature(pressure2metre_standardICAO(pmid_Pa));

    return boxVolume_dry(pmid_Pa, mass_kg, temp_K);
}


double windspeed(double u,
                 double v)
{
    double speed = sqrt(pow(u, 2) + pow(v, 2));
    return speed;
}


double potTemp(double temp_K,
               double pressure_hPa)
{
    // Constants

    double R = MetConstants::GAS_CONSTANT_DRY_AIR;
    double c_p = MetConstants::SPEC_HEAT_DRY_PRESSURE;

    double potTemp_K = temp_K * pow(1000. / pressure_hPa, R / c_p);
    return potTemp_K;
}


double vecLengthZ(double x_m, double y_m, double Z)
{
    double norm = sqrt(pow(x_m,2) + pow(y_m,2) + pow(Z,2));
    return norm;
}


double PVFromT_P_U_V_lat(double u000, double v000, double t000_K, double p000_hPa, double lon000_deg, double lat000_deg,
                         double u010, double t010_K, double p010_hPa, double lon010_deg, double lat010_deg, double q010,
                         double u110, double t110_K, double p110_hPa, double q110,
                         double u_110, double t_110_K, double p_110_hPa, double q_110,
                         double v001, double t001_K, double p001_hPa, double lon001_deg, double lat001_deg, double q001,
                         double v101, double t101_K, double p101_hPa, double q101,
                         double v_101, double t_101_K, double p_101_hPa, double q_101,
                         double t100_K, double p100_hPa)
{
    // Constants

    double radius_m = MetConstants::EARTH_RADIUS_km * 1000.;
    double PVU_factor = MetConstants::PVU_FACTOR;
    double g = MetConstants::GRAVITY_ACCELERATION;

    // Compute potential temperature on required grid points.

    double theta000_K = potTemp(t000_K, p000_hPa);

    double theta001_K = potTemp(t001_K, p001_hPa);
    double theta101_K = potTemp(t101_K, p101_hPa);
    double theta_101_K = potTemp(t_101_K, p_101_hPa);

    double theta010_K = potTemp(t010_K, p010_hPa);
    double theta110_K = potTemp(t110_K, p110_hPa);
    double theta_110_K = potTemp(t_110_K, p_110_hPa);

    // Compute isentropic relative vorticity:
    // (dv/dx)_theta - (du/dy)_theta

        // ... v = v000 + dv_theta (change on theta surface)

        double v;
        bool xslope_positive = false;

        if (theta001_K - theta000_K < 0)
        {
            xslope_positive = true;
            v = v001 + (v101 - v001) * abs((theta001_K - theta000_K) / (theta001_K - theta101_K));
        }
        else
        {
            v = v001 + (v_101 - v001) * abs((theta001_K - theta000_K) / (theta001_K - theta_101_K));
        }

        double dv_theta = v - v000;

        // ... Compute distance via pythagorean theorem:
        // ... dx_theta = { dx^2 + [ (v - v001)/(v101 - v001) * dz ]^2 }^0.5

            // ... ... dx
            double dx = gcDistance_deg(lon000_deg, lat000_deg,
                                       lon001_deg, lat001_deg,
                                       radius_m);

            // ... ... Convert pressure to height difference:
            // ... ... dz = R * T_v * d(lnp), d(lnp) = ln(p001 / p101)

            double T_v_mean_vx = 0.;
            double dPhi_vx = 0.;
            double dz_vx = 0.;
            double dx_theta = 0;

            if (xslope_positive)
            {
                T_v_mean_vx = (virtualTempFromSpecHum(t101_K, q101) + virtualTempFromSpecHum(t001_K, q001)) / 2.;
                dPhi_vx = geopotThickness(T_v_mean_vx, p101_hPa, p001_hPa);
                dz_vx = geopotToZ(dPhi_vx);
                dx_theta = sqrt(pow(dx, 2) + pow((v - v001) / (v101 - v001) * dz_vx, 2));
            }
            else
            {
                T_v_mean_vx = (virtualTempFromSpecHum(t_101_K, q_101) + virtualTempFromSpecHum(t001_K, q001)) / 2.;
                dPhi_vx = geopotThickness(T_v_mean_vx, p001_hPa, p_101_hPa);
                dz_vx = geopotToZ(dPhi_vx);
                dx_theta = sqrt(pow(dx, 2) + pow((v - v001) / (v_101 - v001) * dz_vx, 2));
            }


        // ... u = u000 + du_theta (change on theta surface)

        double u;
        bool yslope_positive = false;

        if (theta010_K - theta000_K < 0)
        {
            yslope_positive = true;
            u = u010 + (u110 - u010) * abs((theta010_K - theta000_K) / (theta010_K - theta110_K));
        }
        else
        {
            u = u010 + (u_110 - u010) * abs((theta010_K - theta000_K) / (theta010_K - theta_110_K));
        }

        double du_theta = u - u000;

        // ... Compute distance via pythagorean theorem:
        // ... dy_theta = { dy^2 + [ (u - u010)/(u110 - u010) * dz ]^2 }^0.5

            // ... ... dy
            double dy = gcDistance_deg(lon000_deg, lat000_deg,
                                       lon010_deg, lat010_deg,
                                       radius_m);

            // ... ... Convert pressure to height difference:
            // ... ... dz = R * T_v * d(lnp), d(lnp) = ln(p001 / p101

            double T_v_mean_uy = 0.;
            double dPhi_uy = 0.;
            double dz_uy = 0.;
            double dy_theta = 0.;

            if(yslope_positive)
            {
                T_v_mean_uy = (virtualTempFromSpecHum(t110_K, q110) + virtualTempFromSpecHum(t010_K, q010)) /2.;
                dPhi_uy = geopotThickness(T_v_mean_uy, p110_hPa, p010_hPa);
                dz_uy = geopotToZ(dPhi_uy);
                dy_theta = sqrt(pow(dy, 2) + pow((u - u010) / (u110 - u010) * dz_uy, 2));
            }
            else
            {
                T_v_mean_uy = (virtualTempFromSpecHum(t_110_K, q_110) + virtualTempFromSpecHum(t010_K, q010)) /2.;
                dPhi_uy = geopotThickness(T_v_mean_uy, p010_hPa, p_110_hPa);
                dz_uy = geopotToZ(dPhi_uy);
                dy_theta = sqrt(pow(dy, 2) + pow((u - u010) / (u_110 - u010) * dz_uy, 2));
            }


    // (dv/dx)_theta - (du/dy)_theta
    double relVo = (dv_theta / dx_theta) - (du_theta / dy_theta);


    // Compute planetary vorticity f = 2 * w_0 * sin(2 * PI * lat / 360).
    double planVo = planetaryVorticity(lat000_deg);

    // Compute vertical theta gradient.

    double dTheta_K = potTemp(t100_K, p100_hPa) - potTemp(t000_K, p000_hPa);
    double dp_Pa = 100. * (p100_hPa - p000_hPa);

    // Compute and return PV.

    double PV = -g * PVU_factor * (planVo + relVo) * (dTheta_K / dp_Pa);
    return PV;
}


double virtualTempFromDewPoint(double dewp_K,
                               double pressure_hPa,
                               double temp_K)
{
    double e_s_hPa = e_satLiquidMagnus_hPa(dewp_K);
    double spec_hum = 0.622 * e_s_hPa / (pressure_hPa + (0.622 - 1.) * e_s_hPa);
    double virtualTemp_K = temp_K * (1 + spec_hum * 0.608);

    return virtualTemp_K;
}


double virtualTempFromSpecHum(double temp_K,
                              double specHum)
{
    double virtualTemp_K = temp_K * (1 + specHum * 0.608);

    return virtualTemp_K;
}


double geopotThickness(double meanVirtualTemp_K,
                       double p_top, double p_bot)
{
    // Constants
    double R = MetConstants::GAS_CONSTANT_DRY_AIR;

    double dln_p = log(p_bot / p_top);
    double dPhi = R * meanVirtualTemp_K * dln_p;

    return dPhi;
}


double geopotToZ(double geopotential)
{
    // Constants
    double g = MetConstants::GRAVITY_ACCELERATION;

    double Z = geopotential / g;

    return Z;
}


double e_satLiquidMagnus_hPa(double temp_K)
{
    double e_sat_hPa = 6.122 * exp(17.62 * (temp_K - 273.15) / (243.12 + temp_K - 273.15));

    return e_sat_hPa;
}


double e_satLiquid_hPa(double temp_K)
{
    double e_sat_hPa = exp(- 0.58002206e4 / temp_K
                       + 0.13914993
                       - 0.48640239e-1 * temp_K
                       + 0.41764768e-4 * pow(temp_K, 2)
                       - 0.14452093e-7 * pow(temp_K, 3)
                       + 0.65459673 * log(temp_K)) / 10000.;

    return e_sat_hPa;
}


double e_satIce_hPa(double temp_K)
{
    // Constants
    double t0_K = MetConstants::IAPWS_MELTING_POINT;

    double ei0_hPa = 6.1071;
    double e_sat_hPa = pow(10.,
                    - 9.09718 * (t0_K / temp_K - 1.)
                    - 3.56654 * log10(t0_K / temp_K)
                    + 0.876793 * (1. - temp_K / t0_K)
                    + log10(ei0_hPa));

    return e_sat_hPa;
}


double theta_e(double temp_K,
               double pressure_hPa,
               double q)
{
    // Constants

    double Lv = MetConstants::LATENT_HEAT_VAPORIZATION_AT100;
    double cp = MetConstants::SPEC_HEAT_DRY_PRESSURE;

    double potTemp_K = potTemp(temp_K, pressure_hPa);

    double dewpoint_K = dewpointApprox(pressure_hPa, q);

    double LCLTemp_K = LCLTempApprox(temp_K, dewpoint_K);

    double mixRatio = q / ( 1. - q );

    double theta_e = potTemp_K * exp( Lv * mixRatio / (cp * LCLTemp_K));

    return theta_e;
}


double dewpointApprox(double pressure_hPa,
                      double specHum)
{
    // Constants
    double epsilon = MetConstants::MOLAR_MASS_WATER / MetConstants::MOLAR_MASS_DRY_AIR;

    double mixRatio = specHum / (1. - specHum);
    double e_hPa = pressure_hPa * mixRatio / (mixRatio + epsilon);
    double dewpoint_K = 243.5 / (17.67 / log( e_hPa / 6.112 ) - 1) + 273.15;

    return dewpoint_K;
}


double LCLTempApprox(double temp_K,
                     double dewpoint_K)
{
    // Constants

    double gammaDry = MetConstants::DRY_ADIABATIC_LAPSE_RATE;
    double gammaDew = MetConstants::DEWPOINT_LAPSE_RATE_APPROX;

    // Compute height difference to LCL using Espy's method:
    // deltaH = T_d - T / ( gammaDew - gammaDry )
    double deltaH = dewpoint_K - temp_K / ( gammaDew - gammaDry );

    // Compute temperature at LCL using dry adiabatic lapse rate again
    double LCLTemp = temp_K + gammaDry * deltaH;

    return LCLTemp;
}


double planetaryVorticity(double lat_deg)
{
    // Constants
    double w_0 = MetConstants::EARTH_ROTATION_RATE;

    double f = 2. * w_0 * sin(2. * M_PI * lat_deg / 360.);

    return f;
}


double linearInterpolation1D(double baseA, double baseB,
                             double valA, double valB,
                             double posX)
{
    double valX = valA + (valB - valA) * abs(posX - baseA) / abs(baseB - baseA);

    return valX;
}

} // namespace Met3D

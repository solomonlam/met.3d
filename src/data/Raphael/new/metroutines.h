/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015 Marc Rautenhaus
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef METROUTINES_H
#define METROUTINES_H

// standard library imports

// related third party imports

// local application imports
#include "util/mutil.h"


namespace Met3D
{

/******************************************************************************
***                               CONSTANTS                                 ***
*******************************************************************************/

// Definition of atmospheric science constants. Reference: e.g. Wallace and
// Hobbs (2006), pp. 467ff.
namespace MetConstants
{
// Universal constants

// Air
const double GAS_CONSTANT_DRY_AIR = 287.058; // J K^-1 kg^-1
const double SPEC_HEAT_DRY_PRESSURE = 1003.; // J K^-1 kg^-1
const double SPEC_HEAT_DRY_VOLUME = 717.; // J K^-1 kg^-1
const double DRY_ADIABATIC_LAPSE_RATE = -0.0098; // K m^-1
const double DEWPOINT_LAPSE_RATE = -0.0018; // K m^-1
const double ISA_LAPSE_RATE = -0.0065; // K m^-1
const double ISA_SEA_LVL_TEMPERATURE = 288.15; // K
const double PVU_FACTOR = 1000000.;  // Multiply to SI-Units to get PV-Units

// Water substance
const double PHASE_CHANGE_SOLID_LIQUID = 273.16; // K
const double LATENT_HEAT_VAPORIZATION_AT0 = 2.50e6; // J kg^-1
const double LATENT_HEAT_VAPORIZATION_AT100 = 2.25e6; // J kg^-1

// Earth and sun
const double GRAVITY_ACCELERATION = 9.80665; // m s^-2
const double EARTH_RADIUS_km = 6371.; // km
const double EARTH_ROTATION_RATE = 0.000072921; // rad s^-1

}


/******************************************************************************
***                                METHODS                                  ***
*******************************************************************************/

inline double degreesToRadians(double angle);


/**
  Computes the haversine: haversin(theta) = sin^2(theta/2.).
  @see https://en.wikipedia.org/wiki/Haversine_formula
 */
inline double haversin(double radians);


double gcDistanceUnitSphere_2(
        const double lon1_rad, const double lat1_rad,
        const double lon2_rad, const double lat2_rad);


/**
  Great circle distance between two points on a unit sphere, in radians.

  Reference: @see http://www.movable-type.co.uk/scripts/gis-faq-5.1.html

  "Presuming a spherical Earth with radius R (see below), and the locations of
  the two points in spherical coordinates (longitude and latitude) are
  lon1,lat1 and lon2,lat2 then the

  Haversine Formula (from R.W. Sinnott, "Virtues of the Haversine", Sky and
  Telescope, vol. 68, no. 2, 1984, p. 159):

        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin^2(dlat/2) + cos(lat1) * cos(lat2) * sin^2(dlon/2)
        c = 2 * arcsin(min(1,sqrt(a)))
        d = R * c

  will give mathematically and computationally exact results. The intermediate
  result c is the great circle distance in radians. The great circle distance d
  will be in the same units as R."

  @see gcDistance()

  @return The intermediate result c (the great circle distance in radians).
 */
double gcDistanceUnitSphere(
        const double lon1_rad, const double lat1_rad,
        const double lon2_rad, const double lat2_rad);


/**
  Great circle distance between two points on a sphere with radius @p radius,
  in the same units as @p radius.
 */
double gcDistance(const double lon1_rad, const double lat1_rad,
                         const double lon2_rad, const double lat2_rad,
                         const double radius);


/**
  Same as @ref gcDistance() but with lon/lat given in degrees.
 */
double gcDistance_deg(const double lon1, const double lat1,
                             const double lon2, const double lat2,
                             const double radius);


inline double cot(double radians);


/**
  Computes the area of a spherical triangle given by three points on a sphere.

  Similar to NCL's gc_tarea() function:
  @ref http://www.ncl.ucar.edu/Document/Functions/Built-in/gc_tarea.shtml
 */
double gcTriangleArea(const double lon1, const double lat1,
                      const double lon2, const double lat2,
                      const double lon3, const double lat3,
                      const double radius);


/**
  Computes the area of of a quadrilateral patch on a sphere of radius @p
  radius.

  Similar to NCL's gc_qarea() function:
  @ref http://www.ncl.ucar.edu/Document/Functions/Built-in/gc_qarea.shtml

  The area is computed by dividing the quadrilateral into two triangles (@see
  gcTriangleArea()) and summing the areas of those triangles.
 */
double gcQuadrilateralArea(const double lon1, const double lat1,
                           const double lon2, const double lat2,
                           const double lon3, const double lat3,
                           const double lon4, const double lat4,
                           const double radius);


/**
    Conversion of pressure (Pa) to geometric height (m) with hydrostatic
    equation, according to the profile of the ICAO standard atmosphere.

    Reference:
        For example, H. Kraus, Die Atmosphaere der Erde, Springer, 2001,
        470pp., Sections II.1.4. and II.6.1.2.

    @param p_Pa pressure (Pa)
    @return geometric elevation in m
 */
double pressure2metre_standardICAO(double p_Pa);


/**
    Conversion of geometric height (given in m) to pressure (Pa) with
    hydrostatic equation, according to the profile of the ICAO standard
    atmosphere.

    Reference:
        For example, H. Kraus, Die Atmosphaere der Erde, Springer, 2001,
        470pp., Sections II.1.4. and II.6.1.2.

    @param z_m elevation in m
    @return pressure in Pa
 */
double metre2pressure_standardICAO(double z_m);


/**
    International standard atmosphere temperature at the given elevation.

    Reference:
        For example, H. Kraus, Die Atmosphaere der Erde, Springer, 2001,
        470pp., Sections II.1.4. and II.6.1.2.

    @param z_m elevation in m
    @return temperature (K)
 */
double isaTemperature(double z_m);


/**
  Convert @p flightlevel in hft to metres.
 */
double flightlevel2metre(double flightlevel);


/**
  Convert @p z_m in m to flightlevel in hft.
 */
double metre2flightlevel(double z_m);


/**
  Computes the mass (kg) of the air in a column with surface area @p area_m2
  (m^2), between bottom and top pressures @p pbot_Pa (Pa) and @p ptop_Pa (Pa).

  Reference:
    Wallace and Hobbs (2006), Atmospheric Science 2nd ed., p 68, Fig. 3.1:
    downward force Fd = mass * grav. accel. == upward force Fu = deltap * area.

  @return airmass (kg)
 */
double columnAirmass(double pbot_Pa, double ptop_Pa, double area_m2);


/**
  Computes the volume (m^3) of a box with air pressure @p p_Pa (Pa), air mass
  @p mass_kg (kg) and air temperature @p temp_K (K). The ideal gas law with
  the gas constant for dry air is used.

  Reference:
    Wallace and Hobbs (2006), Atmospheric Science 2nd ed., Ch. 3.
    Ideal gas law: p*V = m*R*T --> V = m*R*T / p

  @return volume in m^3
 */
double boxVolume_dry(double p_Pa, double mass_kg, double temp_K);


/**
  Computes the volume of a regular lon/lat/pressure grid box.

  If @p temp_K is not specified, the temperature is estimated from the
  international standard atmosphere. @see isaTemperature().

  @return volume in m^3
 */
double boxVolume_dry(double northWestLon, double northWestLat,
                     double southEastLon, double southEastLat,
                     double pmid_Pa, double pbot_Pa, double ptop_Pa,
                     double temp_K = M_MISSING_VALUE);


/**
 Computes windspeed [m/s] from
 horizontal wind components u and v [m/s]
 according to the pythagorean theorem
 */
double windspeed(double u, double v);


/**
 * Computes potential temperature [K] from
 * temperature [K]
 * and pressure [hPa]
 * according to
 * "J.M.Wallace, P.V.Hobbs:
 * Atmospheric Science, An Introductory Survey
 * Second Edition, p.78, Eq. 3.54"
 */
double potTemp(double temp_K, double pressure_hPa);


/**
 * Returns the three components of the gradient of a scalar field,
 * index n, in cartesian coordinates.
 * "#" denotes units of the scalar field.
 * n = 0 Z [#/gpm]
 * n = 1 lat [#/m]
 * n = 2 lon [#/m]
 */
double* gradient(double var000, double lon000_deg, double lat000_deg, // Current grid point
                 double var001, double lon001_deg, // xlon-neighbour
                 double var010, double lat010_deg, // ylat-neighbour
                 double var100, double dZ_m // Z-neighbour
                 );


/**
 * Returns the norm of a vector using geopotential height
 */
double vecLengthZ(double x_m, double y_m, double Z);


// Raphido: add description or reference.
// Raphido: Somewhere add graphical sketch illustrating this method.
//          THIS METHOD IS POSSIBLY INACCURATE AND NEEDS TO BE REVIEWED!
//          Basic assumptions: 1) Theta always increases with height.
//                             2) Theta, u and v vary linearly between adjacent grid points.
double PVFromT_P_U_V_lat(double u000, double v000, double t000_K, double p000_hPa, double lon000_deg, double lat000_deg,
                         double u010, double t010_K, double p010_hPa, double lon010_deg, double lat010_deg, double q010,
                         double u110, double t110_K, double p110_hPa, double q110,
                         double u_110, double t_110_K, double p_110_hPa, double q_110,
                         double v001, double t001_K, double p001_hPa, double lon001_deg, double lat001_deg, double q001,
                         double v101, double t101_K, double p101_hPa, double q101,
                         double v_101, double t_101_K, double p_101_hPa, double q_101,
                         double t100_K, double p100_hPa);


/**
 * Computes virtual temperature [K] from
 * dewpoint temperature [K],
 * pressure [hPa] and
 * temperature [K]
 * according to
 * https://bitbucket.org/wxmetvis/mss/mslib/thermolib.py
 * and using Magnus' formula for e_sat over plain water surfaces.
 * Raphido: add reference for Magnus' formula or use Hyland Wexler
 */
double virtualTempFromDewPoint(double dewp_K,
                               double pressure_hPa,
                               double temp_K);


/**
 * Computes virtual temperature from specific humidity.
 * Raphido: add reference.
 */
double virtualTempFromSpecHum(double temp_K,
                              double specHum);


/**
 * Computes geopotential thickness[J/kg] from
 * mean virtual temperature [K] and
 * pressure ratio [same units!]
 * between current and lower level pressure
 * according to
 * Raphido: Add reference.
 */
double geopotThickness(double meanVirtualTemp_K,
                       double p_top, double p_bot);


/**
 * Converts geopotential thickness [J/kg] to
 * geopotential height [gpm].
 * Note that, for many meteorological cases, the
 * numerical difference between [m] and [gpm]
 * is negligible; for reference see
 * "J.M.Wallace, P.V.Hobbs:
 *  Atmospheric Science, An Introductory Survey
 *  Second Edition, p.69, Table 3.1"
 */
double geopotToZ(double geopotential);


/**
 * Computes saturation pressure [hPa] from
 * temperature [K]
 * according to
 * Magnus' formula for plain water surfaces.
 */
double e_satLiquidMagnus_hPa(double temp_K);


/**
 * Computes saturation pressure over water according to
 * https://bitbucket.org/wxmetvis/mss/mslib/thermolib.py
 * using "HylandWexler" method.
 */
double e_satLiquid_hPa(double temp_K);


/**
 * Computes saturation pressure over ice according to
 * https://bitbucket.org/wxmetvis/mss/mslib/thermolib.py
 * using "GoffGratch" method.
 */
double e_satIce_hPa(double temp_K);


// Raphido: Review this reference. Its probably Holton, really.
/**
 * Computes equivalent potential temperature  from
 * temperature [K]
 * pressure[hPa] and
 * specific humidity [kg/kg]
 * according to
 * "J.M.Wallace, P.V.Hobbs:
 *  Atmospheric Science, An Introductory Survey
 *  Second Edition, p.85, Eq. 3.71"
 */
double theta_e(double temp_K, double pressure_hPa,
               double q);


// Raphido: add description/reference.
double sfcTo1000hPaGeopot(double sfcGeopotential,
                          double sfcPressure_Pa,
                          double sfcTemp_K);



/**
 * Computes dewpoint temperature [K] from
 * pressure [hPa] and
 * specific humidity [kg/kg]
 * according to
 * https://bitbucket.org/wxmetvis/mss/mslib/thermolib.py
 * "dewpoint_approx" method
 */
double dewpointApprox(double pressure_hPa,
                      double specHum);


/**
 * Computes LCL temperature [K] from
 * temperature [K] and
 * dewpoint temperature [K]
 * according to "Espy's approximation"
 * (Raphido : add acceptable reference)
 * Note: This method can be reduced to one step,
 * it is artificially split up to seperate out the
 * "Espy's method" part, which gives the height difference.
 */
double LCLTempApprox(double temp_K,
                     double dewpoint_K);

/*
 * Computes planetary Vorticity [s^-1] from
 * latitude [deg]
 */
double planetaryVorticity(double lat_deg);


double linearInterpolation1D(double baseA, double baseB,
                             double valA, double valB,
                             double posX);


} // namespace Met3D

#endif // METROUTINES_H


// ISENTROPIC POTENTIAL VORTICITY
// Input grids : 0 t, 1 q, 2 u, 3 v, 4 sfcPhi, 5 sfc t_d, 6 sfc t, 7 sfc p, 8 sfc u, 9 sfc v
else if ( (derivedVarName == "isentropic PV (an)")
          || (derivedVarName == "isentropic PV (fc)")
          || (derivedVarName == "isentropic PV (ens)") )
{
    unsigned int k, j, i;
    int n, l;
    #pragma omp parallel
    {
        #pragma omp for private(j, i, n, l)
        for (k = 1; k < derivedGrid->getNumLevels() - 1; k++)
            for (j = 0; j < derivedGrid->getNumLats() - 1; j++)
                for (i = 0; i < derivedGrid->getNumLons() - 1; i++)
        {

        // Compute potential temperature at current grid point ...

        double T000_K = inputGrids.at(0)->getValue(k, j, i);
        double p000_hPa = inputGrids.at(0)->getPressure(k, j, i);
        double q000 = inputGrids.at(1)->getValue(k, j, i);
        double theta000_K = potTemp(T000_K, p000_hPa);

        // ... and its upper neighbour.

        double T100_K = inputGrids.at(0)->getValue(k - 1, j, i);
        double p100_hPa = inputGrids.at(0)->getPressure(k - 1, j, i);
        double q100 = inputGrids.at(1)->getValue(k - 1, j, i);
        double theta100_K = potTemp(T100_K, p100_hPa);

        // Compute geopotential height difference dZ between current point and its upper neighbour.

        double meanVirtualTemp_K = (virtualTempFromSpecHum(T000_K, q000) + virtualTempFromSpecHum(T100_K, q100)) / 2.;
        double dPhi_J = geopotThickness(meanVirtualTemp_K, p100_hPa, p000_hPa);
        double dZ_m = geopotToZ(dPhi_J);


        // Compute geopotential height of current grid point.
        // In addition, compute geopotential heights of x- and y-neighbour at surface and lowest level
        // to later use them for interpolation.

            // Surface geopotential.

            double geopotSfc000_J = inputGrids.at(4)->getValue(0, j, i);
            double geopotSfc010_J = inputGrids.at(4)->getValue(0, j + 1, i);
            double geopotSfc001_J = inputGrids.at(4)->getValue(0, j, i + 1);


            // Surface layer mean virtual temperature,
            // assuming temperature ranging from -45 to +60 degrees Celsius.

            // Surface pressure needs to be converted to hPa.

            double pSfc000_hPa = inputGrids.at(7)->getValue(0, j, i) / 100.;
            double pSfc010_hPa = inputGrids.at(7)->getValue(0, j + 1, i) / 100.;
            double pSfc001_hPa = inputGrids.at(7)->getValue(0, j, i + 1) / 100.;

            double virtualTempSfc000_K = virtualTempFromDewPoint(inputGrids.at(5)->getValue(0, j, i),
                                                                  pSfc000_hPa,
                                                                  inputGrids.at(6)->getValue(0, j, i));

            double virtualTempSfc010_K = virtualTempFromDewPoint(inputGrids.at(5)->getValue(0, j + 1, i),
                                                                  pSfc010_hPa,
                                                                  inputGrids.at(6)->getValue(0, j + 1, i));

            double virtualTempSfc001_K = virtualTempFromDewPoint(inputGrids.at(5)->getValue(0, j, i + 1),
                                                                  pSfc001_hPa,
                                                                  inputGrids.at(6)->getValue(0, j, i + 1));

            double virtualTempFirstLvl000_K = virtualTempFromSpecHum(inputGrids.at(0)->getValue(inputGrids.at(0)->getNumLevels() - 1, j, i),
                                                                     inputGrids.at(1)->getValue(inputGrids.at(1)->getNumLevels() - 1, j, i));

            double virtualTempFirstLvl010_K = virtualTempFromSpecHum(inputGrids.at(0)->getValue(inputGrids.at(0)->getNumLevels() - 1, j + 1, i),
                                                                     inputGrids.at(1)->getValue(inputGrids.at(1)->getNumLevels() - 1, j + 1, i));

            double virtualTempFirstLvl001_K = virtualTempFromSpecHum(inputGrids.at(0)->getValue(inputGrids.at(0)->getNumLevels() - 1, j, i + 1),
                                                                     inputGrids.at(1)->getValue(inputGrids.at(1)->getNumLevels() - 1, j, i + 1));

            double meanVirtualTempAtSfc000_K = (virtualTempSfc000_K + virtualTempFirstLvl000_K) / 2.;
            double meanVirtualTempAtSfc010_K = (virtualTempSfc010_K + virtualTempFirstLvl010_K) / 2.;
            double meanVirtualTempAtSfc001_K = (virtualTempSfc001_K + virtualTempFirstLvl001_K) / 2.;

            // Surface layer geopotential thickness.

            double dPhiSfcLayer000_J = geopotThickness(meanVirtualTempAtSfc000_K,
                                                        inputGrids.at(0)->getPressure(inputGrids.at(0)->getNumLevels() - 1, j, i),
                                                        pSfc000_hPa);

            double dPhiSfcLayer010_J = geopotThickness(meanVirtualTempAtSfc010_K,
                                                        inputGrids.at(0)->getPressure(inputGrids.at(0)->getNumLevels() - 1, j + 1, i),
                                                        pSfc010_hPa);

            double dPhiSfcLayer001_J = geopotThickness(meanVirtualTempAtSfc001_K,
                                                        inputGrids.at(0)->getPressure(inputGrids.at(0)->getNumLevels() - 1, j, i + 1),
                                                        pSfc001_hPa);

            // Geopotential height of current grid point.
            double geopot000_J = 0.;

            double meanVirtualTemp000_K = 0.;

            double dPhi000_J = 0.;

            // For each point, loop from surface level to current level (k).

            for (l = derivedGrid->getNumLevels() - 2; l > k - 1; l--)
            {

                // Compute average virtual temperature between current pressure level
                // and lower level.
                meanVirtualTemp000_K = ( virtualTempFromSpecHum(inputGrids.at(0)->getValue(l, j, i),
                                                                inputGrids.at(1)->getValue(l, j, i))
                                       + virtualTempFromSpecHum(inputGrids.at(0)->getValue(l + 1, j, i),
                                                                inputGrids.at(1)->getValue(l + 1, j, i))
                                       ) / 2.;

                // Compute the current thickness.
                dPhi000_J = geopotThickness(meanVirtualTemp000_K,
                                            inputGrids.at(0)->getPressure(l, j, i),
                                            inputGrids.at(0)->getPressure(l + 1, j, i));

                // Sum up the differentials.
                geopot000_J += dPhi000_J;
            }

            // Add surface layer geopotential thickness
            geopot000_J += dPhiSfcLayer000_J;

            // Add surface geopotential
            geopot000_J += geopotSfc000_J;

            // Convert to geopotential height by dividing through g_0.
            double Z000_m = geopotToZ(geopot000_J);


        // We need the tangential plane of the theta surface at the current grid point.
        // The two vectors spanning this plane are perpendicular to the theta gradient
        // and are defined to be either in the x-Z or y-Z plane.

            // Interpolate theta010 and theta001 to geopotential height of current grid point.
            // These values are used to compute the theta gradient.

            // Interpolation values
            double thetaX_K = 0.;
            double thetaY_K = 0.;

            double geopot010_lower_J = geopotSfc010_J;
            double geopot001_lower_J = geopotSfc001_J;
            double Z010_lower_m = geopotToZ(geopot010_lower_J);
            double Z001_lower_m = geopotToZ(geopot001_lower_J);

            double geopot010_upper_J = geopot010_lower_J + dPhiSfcLayer010_J;
            double geopot001_upper_J = geopot001_lower_J + dPhiSfcLayer001_J;
            double Z010_upper_m = geopotToZ(geopot010_upper_J);
            double Z001_upper_m = geopotToZ(geopot001_upper_J);


            // Find adjacent grid points in x- and y-direction enclosing Z000.
            // Start in y-direction.

            for (n = derivedGrid->getNumLevels() - 1; n > 1; n--)
            {

                    // Check whether adjacent grid points enclose Z000.
                    // If they do, check whether surface is involved or not, then interpolate.

                    if((Z010_lower_m <= Z000_m) && (Z000_m < Z010_upper_m))
                    {

                        // Surface level is involved.

                        if (n == derivedGrid->getNumLevels() - 1)
                        {

                            // Values of theta [K] at upper and lower adjacent grid points.

                            double T_upper_K = inputGrids.at(0)->getValue(n, j + 1, i);
                            double p_upper_hPa = inputGrids.at(0)->getPressure(n, j + 1, i);
                            double theta_upper_K = potTemp(T_upper_K, p_upper_hPa);


                            double T_lower_K = inputGrids.at(6)->getValue(0, j + 1, i);
                            double p_lower_hPa = inputGrids.at(7)->getValue(0, j + 1, i) / 100.;
                            double theta_lower_K = potTemp(T_lower_K, p_lower_hPa);

                            // Interpolated value of theta [K].
                            thetaY_K = linearInterpolation1D(Z010_lower_m, Z010_upper_m,
                                                             theta_lower_K, theta_upper_K,
                                                             Z000_m);

                            break;
                        }

                        // Surface level is not involved.

                        else
                        {
                            // Values of theta [K] at upper and lower adjacent grid points.

                            double T_upper_K = inputGrids.at(0)->getValue(n, j + 1, i);
                            double p_upper_hPa = inputGrids.at(0)->getPressure(n, j + 1, i);
                            double theta_upper_K = potTemp(T_upper_K, p_upper_hPa);


                            double T_lower_K = inputGrids.at(0)->getValue(n + 1, j + 1, i);
                            double p_lower_hPa = inputGrids.at(0)->getPressure(n + 1, j + 1, i);
                            double theta_lower_K = potTemp(T_lower_K, p_lower_hPa);

                            // Interpolated value of theta [K].
                            thetaY_K = linearInterpolation1D(Z010_lower_m, Z010_upper_m,
                                                             theta_lower_K, theta_upper_K,
                                                             Z000_m);

                            break;
                        }

                    }

                    // Z000 is not enclosed between these levels, go one level up.

                    else
                    {
                        // Adjust lower Z.
                        geopot010_lower_J = geopot010_upper_J;
                        Z010_lower_m = geopotToZ(geopot010_lower_J);

                        double virtualTemp_av_K = 0.;
                        double dPhi_J = 0.;



                        // Compute average virtual temperature between current level
                        // and upper neighbour.
                        virtualTemp_av_K = ( virtualTempFromSpecHum(inputGrids.at(0)->getValue(n, j + 1, i),
                                                                    inputGrids.at(1)->getValue(n, j + 1, i))
                                           + virtualTempFromSpecHum(inputGrids.at(0)->getValue(n - 1, j + 1, i),
                                                                    inputGrids.at(1)->getValue(n - 1, j + 1, i)) ) / 2.;

                        // Compute geopotential thickness.
                        dPhi_J = geopotThickness(virtualTemp_av_K,
                                                 inputGrids.at(0)->getPressure(n - 1, j + 1, i),
                                                 inputGrids.at(0)->getPressure(n, j + 1, i));


                        // Adjust upper Z.
                        geopot010_upper_J += dPhi_J;
                        Z010_upper_m = geopotToZ(geopot010_upper_J);
                    }

            }

            // Do the same in x-direction.

            for (n = derivedGrid->getNumLevels() - 1; n > 1; n--)
            {

                    if((Z001_lower_m <= Z000_m) && (Z000_m < Z001_upper_m))
                    {

                        // Surface level is involved.

                        if (n == derivedGrid->getNumLevels() - 1)
                        {

                            // Values of theta [K] at upper and lower adjacent grid points.

                            double T_upper_K = inputGrids.at(0)->getValue(n, j, i + 1);
                            double p_upper_hPa = inputGrids.at(0)->getPressure(n, j, i + 1);
                            double theta_upper_K = potTemp(T_upper_K, p_upper_hPa);


                            double T_lower_K = inputGrids.at(6)->getValue(0, j, i + 1);
                            double p_lower_hPa = inputGrids.at(7)->getValue(0, j, i + 1) / 100.;
                            double theta_lower_K = potTemp(T_lower_K, p_lower_hPa);

                            // Interpolated value of theta [K].
                            thetaX_K = linearInterpolation1D(Z001_lower_m, Z001_upper_m,
                                                             theta_lower_K, theta_upper_K,
                                                             Z000_m);

                            break;
                        }

                        // Surface level is not involved.

                        else
                        {
                            // Values of theta [K] at upper and lower adjacent grid points.

                            double T_upper_K = inputGrids.at(0)->getValue(n, j, i + 1);
                            double p_upper_hPa = inputGrids.at(0)->getPressure(n, j, i + 1);
                            double theta_upper_K = potTemp(T_upper_K, p_upper_hPa);


                            double T_lower_K = inputGrids.at(0)->getValue(n + 1, j, i + 1);
                            double p_lower_hPa = inputGrids.at(0)->getPressure(n + 1, j, i + 1);
                            double theta_lower_K = potTemp(T_lower_K, p_lower_hPa);

                            // Interpolated value of theta [K].
                            thetaX_K = linearInterpolation1D(Z001_lower_m, Z001_upper_m,
                                                             theta_lower_K, theta_upper_K,
                                                             Z000_m);

                            break;
                        }

                    }

                    // Z000 is not enclosed between these levels, go one level up.

                    else
                    {
                        geopot001_lower_J = geopot001_upper_J;
                        Z001_lower_m = geopotToZ(geopot001_lower_J);

                        double virtualTemp_av_K = 0.;
                        double dPhi_J = 0.;


                        virtualTemp_av_K = ( virtualTempFromSpecHum(inputGrids.at(0)->getValue(n, j, i + 1),
                                                                    inputGrids.at(1)->getValue(n, j, i + 1))
                                           + virtualTempFromSpecHum(inputGrids.at(0)->getValue(n - 1, j, i + 1),
                                                                    inputGrids.at(1)->getValue(n - 1, j, i + 1)) ) / 2.;

                        dPhi_J = geopotThickness(virtualTemp_av_K,
                                                 inputGrids.at(0)->getPressure(n - 1, j, i + 1),
                                                 inputGrids.at(0)->getPressure(n, j, i + 1));


                        geopot001_upper_J += dPhi_J;
                        Z001_upper_m = geopotToZ(geopot001_upper_J);
                    }

            }


            // The vertical components of the tangent vectors are determined
            // from the two requirements stated above.

            double dZ_x_m = (theta000_K - thetaX_K) * (theta100_K - theta000_K) / dZ_m;

            double dZ_y_m = dZ_x_m * (thetaY_K - theta000_K) / (thetaX_K - theta000_K);


        // From the current grid point, the tangent vectors now point to locations only
        // vertically displaced from the x- or y-neighbour, respectively.
        // The next step is to retrieve the wind components at these locations to compute
        // relative vorticity.

        // Find levels whose Z values enclose the geopotential height values
        // of the locations mentioned above and interpolate u and v to these values,
        // respectively.
        // The geopotential heights to interpolate are found by adding the gradients' Z-component
        // to the current grid points' geopotential height.
        // The interpolation scheme is the same as for theta.

                // x-coordinate
                double vdx = 0.; // Interpolated value of v [m/s].
                double Z_x_m = Z000_m + dZ_x_m; // Interpolation value of Z.

                // Surface layer geopotential already computed.

                // Lower neighbour.

                double geopot001_J_lower = geopotSfc001_J;
                double Zx_m_lower = geopotToZ(geopot001_J_lower);

                // Upper neighbour.

                double geopot001_J_upper = geopot001_J_lower + dPhiSfcLayer001_J;
                double Zx_m_upper = geopotToZ(geopot001_J_upper);


                for (n = derivedGrid->getNumLevels() - 1; n > 1; n--)
                {

                        if ((Zx_m_lower <= Z_x_m) && (Z_x_m < Zx_m_upper))
                        {

                            // Surface level is involved.

                            if (n == derivedGrid->getNumLevels() - 1)
                            {
                                // Values of v [m/s] at upper and lower adjacent grid points.
                                double v_top = inputGrids.at(3)->getValue(inputGrids.at(3)->getNumLevels() - 1, j, i + 1);
                                double v_bot = inputGrids.at(9)->getValue(0, j, i + 1);

                                // Interpolated value of v.
                                vdx = v_bot + (v_top - v_bot) * (Z_x_m - Zx_m_lower) / (Zx_m_upper - Zx_m_lower);

                                break;
                            }

                            // Surface level is not involved.

                            else
                            {
                                // Values of v [m/s] at upper and lower adjacent grid points.
                                double v_top = inputGrids.at(3)->getValue(n, j, i + 1);
                                double v_bot = inputGrids.at(3)->getValue(n + 1, j, i + 1);

                                // Interpolated value of v.
                                vdx = v_bot + (v_top - v_bot) * (Z_x_m - Zx_m_lower) / (Zx_m_upper - Zx_m_lower);

                                break;
                            }

                        }

                        // Z_x is not enclosed between these levels, go one level up.

                        else
                        {
                            geopot001_J_lower = geopot001_J_upper;
                            Zx_m_lower = geopotToZ(geopot001_J_lower);

                            double virtualTemp_av_K = 0.;
                            double dPhi_J = 0.;

                            virtualTemp_av_K = ( virtualTempFromSpecHum(inputGrids.at(0)->getValue(n, j, i + 1),
                                                                        inputGrids.at(1)->getValue(n, j, i + 1))
                                               + virtualTempFromSpecHum(inputGrids.at(0)->getValue(n - 1, j, i + 1),
                                                                        inputGrids.at(1)->getValue(n - 1, j, i + 1)) ) / 2.;

                            dPhi_J = geopotThickness(virtualTemp_av_K,
                                                     inputGrids.at(0)->getPressure(n - 1, j, i + 1),
                                                     inputGrids.at(0)->getPressure(n, j, i + 1));


                            geopot001_J_upper += dPhi_J;
                            Zx_m_upper = geopotToZ(geopot001_J_upper);
                        }

                }

                // y-coordinate

                // (3c) Reuse geopotential height of current grid point and add Z component
                // of y-Z-plane tangent vector.

                double udy = 0.; // Interpolated value of u [m/s].
                double Z_y_m = Z000_m + dZ_y_m; // Interpolation value of Z.


                // Surface layer geopotential already computed.

                // Lower neighbour.

                double geopot010_J_lower = geopotSfc010_J;
                double Zy_lower_m = geopotToZ(geopot010_J_lower);

                // Upper neighbour.

                double geopot010_J_upper = geopot010_J_lower + dPhiSfcLayer010_J;
                double Zy_upper_m = geopotToZ(geopot010_J_upper);

                for (n = derivedGrid->getNumLevels() - 1; n > 1; n--)
                {

                        if((Zy_lower_m <= Z_y_m) && (Z_y_m < Zy_upper_m))
                        {

                            // Surface level is involved.

                            if (n == derivedGrid->getNumLevels() - 1)
                            {
                                // Values of u [m/s] at upper and lower adjacent grid points.
                                double u_top = inputGrids.at(2)->getValue(inputGrids.at(2)->getNumLevels() - 1, j + 1, i);
                                double u_bot = inputGrids.at(8)->getValue(0, j + 1, i);

                                // Interpolated value of u [m/s].
                                udy = u_bot + (u_top - u_bot) * (Z_y_m - Zy_lower_m) / (Zy_upper_m - Zy_lower_m);

                                break;
                            }

                            // Surface level is not involved.

                            else
                            {
                                // Values of u [m/s] at upper and lower adjacent grid points.
                                double u_top = inputGrids.at(2)->getValue(n, j + 1, i);
                                double u_bot = inputGrids.at(2)->getValue(n + 1, j + 1, i);

                                // Interpolated value of u [m/s].
                                udy = u_bot + (u_top - u_bot) * (Z_y_m - Zy_lower_m) / (Zy_upper_m - Zy_lower_m);

                                break;
                            }

                        }

                        // Z_y is not enclosed between these levels, go one level up.

                        else
                        {
                            geopot010_J_lower = geopot010_J_upper;
                            Zy_lower_m = geopotToZ(geopot010_J_lower);

                            double virtualTemp_av_K = 0.;
                            double dPhi_J = 0.;

                            virtualTemp_av_K = ( virtualTempFromSpecHum(inputGrids.at(0)->getValue(n, j + 1, i),
                                                                        inputGrids.at(1)->getValue(n, j + 1, i))
                                               + virtualTempFromSpecHum(inputGrids.at(0)->getValue(n - 1, j + 1, i),
                                                                        inputGrids.at(1)->getValue(n - 1, j + 1, i)) ) / 2.;

                            dPhi_J = geopotThickness(virtualTemp_av_K,
                                                     inputGrids.at(0)->getPressure(n - 1, j + 1, i),
                                                     inputGrids.at(0)->getPressure(n, j + 1, i));


                            geopot010_J_upper += dPhi_J;
                            Zy_upper_m = geopotToZ(geopot010_J_upper);
                        }

                }

            // Compute geometrical length of tangent vectors.

            double radius_m = MetConstants::EARTH_RADIUS_km * 1000.;

            double dx_m = gcDistance_deg(inputGrids.at(0)->getEastInterfaceLon(i),
                                         inputGrids.at(0)->getNorthInterfaceLat(j),
                                         inputGrids.at(0)->getEastInterfaceLon(i + 1),
                                         inputGrids.at(0)->getNorthInterfaceLat(j),
                                         radius_m);

            double dy_m = gcDistance_deg(inputGrids.at(0)->getEastInterfaceLon(i),
                                         inputGrids.at(0)->getNorthInterfaceLat(j),
                                         inputGrids.at(0)->getEastInterfaceLon(i),
                                         inputGrids.at(0)->getNorthInterfaceLat(j + 1),
                                         radius_m);

            double dx_Z_m = vecLengthZ(dx_m, 0., dZ_x_m);

            double dy_Z_m = vecLengthZ(0., dy_m, dZ_y_m);

            // Compute wind differences [m/s]

            double du = udy - inputGrids.at(2)->getValue(k, j, i);
            double dv = vdx - inputGrids.at(3)->getValue(k, j, i);

            // Compute isentropic relative vorticity [s^-1].
            double vo_rel = (dv / dx_Z_m) - (du / dy_Z_m);

            // Compute values for static stability [K/Pa].

            double dTheta = theta100_K - theta000_K;
            double dp = 100. * (p100_hPa - p000_hPa);

            // Compute planetary vorticity f = 2 * w_0 * sin(2 * PI * lat / 360) [s^-1].

            double lat000_deg = inputGrids.at(0)->getNorthInterfaceLat(j);
            double f = planetaryVorticity(lat000_deg);

            // Compute PV[pvu].

            double factor = MetConstants::PVU_FACTOR;
            double g = MetConstants::GRAVITY_ACCELERATION;
            double staticStability = dTheta / dp;
            double PV_pvu = -factor * g * (vo_rel + f) * staticStability;

            derivedGrid->setValue(k, j, i, PV_pvu);
        }
    } //end pragma
}


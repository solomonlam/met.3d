/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015 Marc Rautenhaus
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "derivedmetvarsdatasource.h"

// standard library imports
#include <iostream>
#include "assert.h"

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "util/metroutines.h"

using namespace std;

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MDerivedMetVarsDataSource::MDerivedMetVarsDataSource()
    : MStructuredGridEnsembleFilter(),
      inputSource(nullptr)
{
//TODO (mr, 2016May17) -- this table should be put into a configuration file
    requiredInputVariablesList["windspeed (an)"] =
            (QStringList() << "u (an)/HYBRID_SIGMA_PRESSURE_3D"
                           << "v (an)/HYBRID_SIGMA_PRESSURE_3D");
    requiredInputVariablesList["windspeed (fc)"] =
            (QStringList() << "u (fc)/HYBRID_SIGMA_PRESSURE_3D"
                           << "v (fc)/HYBRID_SIGMA_PRESSURE_3D");
    requiredInputVariablesList["windspeed (ens)"] =
            (QStringList() << "u (ens)/HYBRID_SIGMA_PRESSURE_3D"
                           << "v (ens)/HYBRID_SIGMA_PRESSURE_3D");


    requiredInputVariablesList["potential temperature (an)"] =
            (QStringList() << "t (an)/HYBRID_SIGMA_PRESSURE_3D");
    requiredInputVariablesList["potential temperature (fc)"] =
            (QStringList() << "t (fc)/HYBRID_SIGMA_PRESSURE_3D");
    requiredInputVariablesList["potential temperature (ens)"] =
            (QStringList() << "t (ens)/HYBRID_SIGMA_PRESSURE_3D");


    requiredInputVariablesList["equivalent potential temperature (an)"] =
            (QStringList() << "t (an)/HYBRID_SIGMA_PRESSURE_3D"
                           << "q (an)/HYBRID_SIGMA_PRESSURE_3D");
    requiredInputVariablesList["equivalent potential temperature (fc)"] =
            (QStringList() << "t (fc)/HYBRID_SIGMA_PRESSURE_3D"
                           << "q (fc)/HYBRID_SIGMA_PRESSURE_3D");
    requiredInputVariablesList["equivalent potential temperature (ens)"] =
            (QStringList() << "t (ens)/HYBRID_SIGMA_PRESSURE_3D"
                           << "q (ens)/HYBRID_SIGMA_PRESSURE_3D");


    requiredInputVariablesList["isentropic PV (simplified) (an)"] =
            (QStringList() << "t (an)/HYBRID_SIGMA_PRESSURE_3D"
                           << "q (an)/HYBRID_SIGMA_PRESSURE_3D"
                           << "u (an)/HYBRID_SIGMA_PRESSURE_3D"
                           << "v (an)/HYBRID_SIGMA_PRESSURE_3D");
    requiredInputVariablesList["isentropic PV (simplified) (fc)"] =
            (QStringList() << "t (fc)/HYBRID_SIGMA_PRESSURE_3D"
                           << "q (fc)/HYBRID_SIGMA_PRESSURE_3D"
                           << "u (fc)/HYBRID_SIGMA_PRESSURE_3D"
                           << "v (fc)/HYBRID_SIGMA_PRESSURE_3D");
    requiredInputVariablesList["isentropic PV (simplified) (ens)"] =
            (QStringList() << "t (ens)/HYBRID_SIGMA_PRESSURE_3D"
                           << "q (ens)/HYBRID_SIGMA_PRESSURE_3D"
                           << "u (ens)/HYBRID_SIGMA_PRESSURE_3D"
                           << "v (ens)/HYBRID_SIGMA_PRESSURE_3D");


    requiredInputVariablesList["isentropic PV (an)"] =
            (QStringList() << "t (an)/HYBRID_SIGMA_PRESSURE_3D"
                           << "q (an)/HYBRID_SIGMA_PRESSURE_3D"
                           << "u (an)/HYBRID_SIGMA_PRESSURE_3D"
                           << "v (an)/HYBRID_SIGMA_PRESSURE_3D"
                           << "z (an)/SURFACE_2D"
                           << "2d (an)/SURFACE_2D"
                           << "2t (an)/SURFACE_2D"
                           << "sp (an)/SURFACE_2D"
                           << "10u (an)/SURFACE_2D"
                           << "10v (an)/SURFACE_2D");
    requiredInputVariablesList["isentropic PV (fc)"] =
            (QStringList() << "t (fc)/HYBRID_SIGMA_PRESSURE_3D"
                           << "q (fc)/HYBRID_SIGMA_PRESSURE_3D"
                           << "u (fc)/HYBRID_SIGMA_PRESSURE_3D"
                           << "v (fc)/HYBRID_SIGMA_PRESSURE_3D"
                           << "z (fc)/SURFACE_2D"
                           << "2d (fc)/SURFACE_2D"
                           << "2t (fc)/SURFACE_2D"
                           << "sp (fc)/SURFACE_2D"
                           << "10u (fc)/SURFACE_2D"
                           << "10v (fc)/SURFACE_2D");
    requiredInputVariablesList["isentropic PV (ens)"] =
            (QStringList() << "t (ens)/HYBRID_SIGMA_PRESSURE_3D"
                           << "q (ens)/HYBRID_SIGMA_PRESSURE_3D"
                           << "u (ens)/HYBRID_SIGMA_PRESSURE_3D"
                           << "v (ens)/HYBRID_SIGMA_PRESSURE_3D"
                           << "z (ens)/SURFACE_2D"
                           << "2d (ens)/SURFACE_2D"
                           << "2t (ens)/SURFACE_2D"
                           << "sp (ens)/SURFACE_2D"
                           << "10u (ens)/SURFACE_2D"
                           << "10v (ens)/SURFACE_2D");


    requiredInputVariablesList["geopotential height (an)"] =
            (QStringList() << "t (an)/HYBRID_SIGMA_PRESSURE_3D"
                           << "q (an)/HYBRID_SIGMA_PRESSURE_3D"
                           << "z (an)/SURFACE_2D"
                           << "2d (an)/SURFACE_2D"
                           << "2t (an)/SURFACE_2D"
                           << "sp (an)/SURFACE_2D");
    requiredInputVariablesList["geopotential height (fc)"] =
            (QStringList() << "t (fc)/HYBRID_SIGMA_PRESSURE_3D"
                           << "q (fc)/HYBRID_SIGMA_PRESSURE_3D"
                           << "z (fc)/SURFACE_2D"
                           << "2d (fc)/SURFACE_2D"
                           << "2t (fc)/SURFACE_2D"
                           << "sp (fc)/SURFACE_2D");
    requiredInputVariablesList["geopotential height (ens)"] =
            (QStringList() << "t (ens)/HYBRID_SIGMA_PRESSURE_3D"
                           << "q (ens)/HYBRID_SIGMA_PRESSURE_3D"
                           << "z (ens)/SURFACE_2D"
                           << "2d (ens)/SURFACE_2D"
                           << "2t (ens)/SURFACE_2D"
                           << "sp (ens)/SURFACE_2D");


    requiredInputVariablesList["1000hPa Z (an)"] =
            (QStringList() << "z (an)/SURFACE_2D"
                           << "sp (an)/SURFACE_2D"
                           << "2t (an)/SURFACE_2D");
    requiredInputVariablesList["1000hPa Z (fc)"] =
            (QStringList() << "z (fc)/SURFACE_2D"
                           << "sp (fc)/SURFACE_2D"
                           << "2t (fc)/SURFACE_2D");
    requiredInputVariablesList["1000hPa Z (ens)"] =
            (QStringList() << "z (ens)/SURFACE_2D"
                           << "sp (ens)/SURFACE_2D"
                           << "2t (ens)/SURFACE_2D");


}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MDerivedMetVarsDataSource::setInputSource(MWeatherPredictionDataSource* s)
{
    inputSource = s;
    registerInputSource(inputSource);
//    enablePassThrough(s);
}


MStructuredGrid* MDerivedMetVarsDataSource::produceData(MDataRequest request)
{
    assert(inputSource != nullptr);

    // Parse request.
    MDataRequestHelper rh(request);
    QString derivedVarName = rh.value("VARIABLE");
    rh.removeAll(locallyRequiredKeys());

    // Get input fields.
    QList<MStructuredGrid*> inputGrids;
    foreach (QString requiredVar, requiredInputVariablesList[derivedVarName])
    {
        QStringList requiredVarDef = requiredVar.split("/");
        QString varName = requiredVarDef.at(0);
        MVerticalLevelType levelType = MStructuredGrid::verticalLevelTypeFromConfigString(requiredVarDef.at(1));
        rh.insert("VARIABLE", varName);
        rh.insert("LEVELTYPE", levelType);


       inputGrids << inputSource->getData(rh.request());

    }

    // Initialize result grid.
    MStructuredGrid *derivedGrid = nullptr;
    if ( !inputGrids.isEmpty() && inputGrids.at(0) != nullptr )
    {
        derivedGrid = createAndInitializeResultGrid(inputGrids.at(0));
    }
    else return nullptr;

    // Compute derived grid.

    // WINDSPEED
    if ( (derivedVarName == "windspeed (an)")
         || (derivedVarName == "windspeed (fc)")
         || (derivedVarName == "windspeed (ens)") )
    {
        unsigned int n;
        #pragma omp parallel
        {

            #pragma omp for
            for (n = 0; n < derivedGrid->getNumValues(); n++)
            {
                float windspeed_ms = windspeed(inputGrids.at(0)->getValue(n),
                                               inputGrids.at(1)->getValue(n));

                derivedGrid->setValue(n, windspeed_ms);
            }
        }
    }


    // POTENTIAL TEMPERATURE
    else if ( (derivedVarName == "potential temperature (an)")
              || (derivedVarName == "potential temperature (fc)")
              || (derivedVarName == "potential temperature (ens)")
              || (derivedVarName == "potential temperature raphi"))
    {
        unsigned int k, j, i;
        #pragma omp parallel
        {
            #pragma omp for private(j, i)
            for (k = 0; k < derivedGrid->getNumLevels(); k++)
                for (j = 0; j < derivedGrid->getNumLats(); j++)
                    for (i = 0; i < derivedGrid->getNumLons(); i++)
            {
                float theta_K = potTemp(inputGrids.at(0)->getValue(k, j, i),
                                        inputGrids.at(0)->getPressure(k, j, i));


                derivedGrid->setValue(k, j, i, theta_K);
            }
        }
    }


    // EQUIVALENT POTENTIAL TEMPERATURE
    else if ( (derivedVarName == "equivalent potential temperature (an)")
              || (derivedVarName == "equivalent potential temperature (fc)")
              || (derivedVarName == "equivalent potential temperature (ens)") )
    {
        unsigned int k, j, i;
        #pragma omp parallel
        {
            #pragma omp for private(j, i)
            for (k = 0; k < derivedGrid->getNumLevels(); k++)
                for (j = 0; j < derivedGrid->getNumLats(); j++)
                    for (i = 0; i < derivedGrid->getNumLons(); i++)
            {
                float thetaE_K = theta_e(inputGrids.at(0)->getValue(k, j, i),
                                         inputGrids.at(0)->getPressure(k, j, i),
                                         inputGrids.at(1)->getValue(k, j, i));

                derivedGrid->setValue(k, j, i, thetaE_K);
            }
        }
    }


    // ISENTROPIC POTENTIAL VORTICITY
    // Input grids : 0 t, 1 q, 2 u, 3 v, 4 sfcPhi, 5 sfc t_d, 6 sfc t, 7 sfc p, 8 sfc u, 9 sfc v
    else if ( (derivedVarName == "isentropic PV (an)")
              || (derivedVarName == "isentropic PV (fc)")
              || (derivedVarName == "isentropic PV (ens)") )
    {
        unsigned int k, j, i;//lev,lat,long?
        int n, l;//storing total number of vertical levels
        //double ***geopotheight;
        //geopotheight = new double[derivedGrid->getNumLevels()][derivedGrid->getNumLats()][derivedGrid->getNumLons()];
        #pragma omp parallel
        {
            #pragma omp for private(j, i, n, l)
            for (k = 1; k < derivedGrid->getNumLevels() - 1; k++)
                for (j = 0; j < derivedGrid->getNumLats() - 1; j++)
                    for (i = 0; i < derivedGrid->getNumLons() - 1; i++)
            {

            // Compute potential temperature at current grid point ...

            double T000_K = inputGrids.at(0)->getValue(k, j, i);
            double p000_hPa = inputGrids.at(0)->getPressure(k, j, i);
            double q000 = inputGrids.at(1)->getValue(k, j, i);
            double theta000_K = potTemp(T000_K, p000_hPa);

            // ... and its upper neighbour.

            double T100_K = inputGrids.at(0)->getValue(k - 1, j, i);
            double p100_hPa = inputGrids.at(0)->getPressure(k - 1, j, i);
            double q100 = inputGrids.at(1)->getValue(k - 1, j, i);
            double theta100_K = potTemp(T100_K, p100_hPa);

            // Compute geopotential height difference dZ between current point and its upper neighbour.

            double meanVirtualTemp_K = (virtualTempFromSpecHum(T000_K, q000) + virtualTempFromSpecHum(T100_K, q100)) / 2.;
            double dPhi_J = geopotThickness(meanVirtualTemp_K, p100_hPa, p000_hPa);
            double dZ_m = geopotToZ(dPhi_J);


            // Compute geopotential height of current grid point.
            // In addition, compute geopotential heights of x- and y-neighbour at surface and lowest level
            // to later use them for interpolation.

                // Surface geopotential.

                double geopotSfc000_J = inputGrids.at(4)->getValue(0, j, i);
                double geopotSfc010_J = inputGrids.at(4)->getValue(0, j + 1, i);
                double geopotSfc001_J = inputGrids.at(4)->getValue(0, j, i + 1);


                // Surface layer mean virtual temperature,
                // assuming temperature ranging from -45 to +60 degrees Celsius.

                // Surface pressure needs to be converted to hPa.

                double pSfc000_hPa = inputGrids.at(7)->getValue(0, j, i) / 100.;
                double pSfc010_hPa = inputGrids.at(7)->getValue(0, j + 1, i) / 100.;
                double pSfc001_hPa = inputGrids.at(7)->getValue(0, j, i + 1) / 100.;

                double virtualTempSfc000_K = virtualTempFromDewPoint(inputGrids.at(5)->getValue(0, j, i),
                                                                      pSfc000_hPa,
                                                                      inputGrids.at(6)->getValue(0, j, i));

                double virtualTempSfc010_K = virtualTempFromDewPoint(inputGrids.at(5)->getValue(0, j + 1, i),
                                                                      pSfc010_hPa,
                                                                      inputGrids.at(6)->getValue(0, j + 1, i));

                double virtualTempSfc001_K = virtualTempFromDewPoint(inputGrids.at(5)->getValue(0, j, i + 1),
                                                                      pSfc001_hPa,
                                                                      inputGrids.at(6)->getValue(0, j, i + 1));

                double virtualTempFirstLvl000_K = virtualTempFromSpecHum(inputGrids.at(0)->getValue(inputGrids.at(0)->getNumLevels() - 1, j, i),
                                                                         inputGrids.at(1)->getValue(inputGrids.at(1)->getNumLevels() - 1, j, i));

                double virtualTempFirstLvl010_K = virtualTempFromSpecHum(inputGrids.at(0)->getValue(inputGrids.at(0)->getNumLevels() - 1, j + 1, i),
                                                                         inputGrids.at(1)->getValue(inputGrids.at(1)->getNumLevels() - 1, j + 1, i));

                double virtualTempFirstLvl001_K = virtualTempFromSpecHum(inputGrids.at(0)->getValue(inputGrids.at(0)->getNumLevels() - 1, j, i + 1),
                                                                         inputGrids.at(1)->getValue(inputGrids.at(1)->getNumLevels() - 1, j, i + 1));

                double meanVirtualTempAtSfc000_K = (virtualTempSfc000_K + virtualTempFirstLvl000_K) / 2.;
                double meanVirtualTempAtSfc010_K = (virtualTempSfc010_K + virtualTempFirstLvl010_K) / 2.;
                double meanVirtualTempAtSfc001_K = (virtualTempSfc001_K + virtualTempFirstLvl001_K) / 2.;

                // Surface layer geopotential thickness.

                double dPhiSfcLayer000_J = geopotThickness(meanVirtualTempAtSfc000_K,
                                                            inputGrids.at(0)->getPressure(inputGrids.at(0)->getNumLevels() - 1, j, i),
                                                            pSfc000_hPa);

                double dPhiSfcLayer010_J = geopotThickness(meanVirtualTempAtSfc010_K,
                                                            inputGrids.at(0)->getPressure(inputGrids.at(0)->getNumLevels() - 1, j + 1, i),
                                                            pSfc010_hPa);

                double dPhiSfcLayer001_J = geopotThickness(meanVirtualTempAtSfc001_K,
                                                            inputGrids.at(0)->getPressure(inputGrids.at(0)->getNumLevels() - 1, j, i + 1),
                                                            pSfc001_hPa);

                // Geopotential height of current grid point.
                double geopot000_J = 0.;

                double meanVirtualTemp000_K = 0.;

                double dPhi000_J = 0.;

                // For each point, loop from surface level to current level (k).

                for (l = derivedGrid->getNumLevels() - 2; l > k - 1; l--)
                {

                    // Compute average virtual temperature between current pressure level
                    // and lower level.
                    meanVirtualTemp000_K = ( virtualTempFromSpecHum(inputGrids.at(0)->getValue(l, j, i),
                                                                    inputGrids.at(1)->getValue(l, j, i))
                                           + virtualTempFromSpecHum(inputGrids.at(0)->getValue(l + 1, j, i),
                                                                    inputGrids.at(1)->getValue(l + 1, j, i))
                                           ) / 2.;

                    // Compute the current thickness.
                    dPhi000_J = geopotThickness(meanVirtualTemp000_K,
                                                inputGrids.at(0)->getPressure(l, j, i),
                                                inputGrids.at(0)->getPressure(l + 1, j, i));

                    // Sum up the differentials.
                    geopot000_J += dPhi000_J;
                }

                // Add surface layer geopotential thickness
                geopot000_J += dPhiSfcLayer000_J;

                // Add surface geopotential
                geopot000_J += geopotSfc000_J;

                // Convert to geopotential height by dividing through g_0.
                double Z000_m = geopotToZ(geopot000_J);//Geopotential height of current grid point


            // We need the tangential plane of the theta surface at the current grid point.
            // The two vectors spanning this plane are perpendicular to the theta gradient
            // and are defined to be either in the x-Z or y-Z plane.

                // Calculate theta?10 and theta?01 at the geopotential height same as the current grid point.
                // They are linearly interpolated from thetas at neighbouring model levels.
                // Find the model levels at neighbouring points that enclosed the geopotential height of current grid point.
                //
                // These values are used to compute the theta gradient and thus compute the dx_theta, dy_theta vectors.
                // Where they span the isentropic surface

                // Interpolation values
                double thetaX_K = 0.;
                double thetaY_K = 0.;

                double geopot010_lower_J = geopotSfc010_J;//Geopotential of the lower level in y+dy
                double geopot001_lower_J = geopotSfc001_J;
                double Z010_lower_m = geopotToZ(geopot010_lower_J);//Geopotential height
                double Z001_lower_m = geopotToZ(geopot001_lower_J);

                double geopot010_upper_J = geopot010_lower_J + dPhiSfcLayer010_J;//Geopotential of the upper level x+dx in model grid
                double geopot001_upper_J = geopot001_lower_J + dPhiSfcLayer001_J;
                double Z010_upper_m = geopotToZ(geopot010_upper_J);
                double Z001_upper_m = geopotToZ(geopot001_upper_J);


                // Find adjacent grid points in x-(?01) and y- (?10) direction enclosing Z000.
                // Consider vertical coordinates only.
                // Start in y-direction.

                for (n = derivedGrid->getNumLevels() - 1; n > 1; n--)//n= number of levels e.g. n= 137 - 1
                {

                        // Check whether adjacent grid points enclose Z000.
                        // If they do, check whether surface is involved or not, then interpolate.

                        if((Z010_lower_m <= Z000_m) && (Z000_m < Z010_upper_m))
                        {

                            // Surface level is involved.

                            if (n == derivedGrid->getNumLevels() - 1)
                            {

                                // Values of theta [K] at upper and lower adjacent grid points.

                                double T_upper_K = inputGrids.at(0)->getValue(n, j + 1, i);
                                double p_upper_hPa = inputGrids.at(0)->getPressure(n, j + 1, i);
                                double theta_upper_K = potTemp(T_upper_K, p_upper_hPa);


                                double T_lower_K = inputGrids.at(6)->getValue(0, j + 1, i);
                                //If surface is involved take surface temp from a different source
                                double p_lower_hPa = inputGrids.at(7)->getValue(0, j + 1, i) / 100.;
                                //surface pressure
                                double theta_lower_K = potTemp(T_lower_K, p_lower_hPa);

                                // Interpolated value of theta [K].
                                thetaY_K = linearInterpolation1D(Z010_lower_m, Z010_upper_m,
                                                                 theta_lower_K, theta_upper_K,
                                                                 Z000_m);

                                break;
                            }

                            // Surface level is not involved.

                            else
                            {
                                // Values of theta [K] at upper and lower adjacent grid points.

                                double T_upper_K = inputGrids.at(0)->getValue(n, j + 1, i);
                                double p_upper_hPa = inputGrids.at(0)->getPressure(n, j + 1, i);
                                double theta_upper_K = potTemp(T_upper_K, p_upper_hPa);


                                double T_lower_K = inputGrids.at(0)->getValue(n + 1, j + 1, i);
                                double p_lower_hPa = inputGrids.at(0)->getPressure(n + 1, j + 1, i);
                                double theta_lower_K = potTemp(T_lower_K, p_lower_hPa);

                                // Interpolated value of theta [K].
                                thetaY_K = linearInterpolation1D(Z010_lower_m, Z010_upper_m,
                                                                 theta_lower_K, theta_upper_K,
                                                                 Z000_m);

                                break;
                            }

                        }

                        // Z000 is not enclosed between these levels, go one level up. i.e. n=n-1

                        else//May calculate for the top level only and save for the rest?
                        {
                            // Replace lower level Z by upper level Z.
                            geopot010_lower_J = geopot010_upper_J;
                            Z010_lower_m = geopotToZ(geopot010_lower_J);

                            double virtualTemp_av_K = 0.;
                            double dPhi_J = 0.;



                            // Compute average virtual temperature between current level
                            // and upper neighbour.
                            virtualTemp_av_K = ( virtualTempFromSpecHum(inputGrids.at(0)->getValue(n, j + 1, i),
                                                                        inputGrids.at(1)->getValue(n, j + 1, i))
                                               + virtualTempFromSpecHum(inputGrids.at(0)->getValue(n - 1, j + 1, i),
                                                                        inputGrids.at(1)->getValue(n - 1, j + 1, i)) ) / 2.;

                            // Compute geopotential thickness.
                            dPhi_J = geopotThickness(virtualTemp_av_K,
                                                     inputGrids.at(0)->getPressure(n - 1, j + 1, i),
                                                     inputGrids.at(0)->getPressure(n, j + 1, i));


                            // Adjust upper Z.
                            geopot010_upper_J += dPhi_J;
                            Z010_upper_m = geopotToZ(geopot010_upper_J);
                        }

                }

                // Do the same in x-direction.

                for (n = derivedGrid->getNumLevels() - 1; n > 1; n--)
                {

                        if((Z001_lower_m <= Z000_m) && (Z000_m < Z001_upper_m))
                        {

                            // Surface level is involved.

                            if (n == derivedGrid->getNumLevels() - 1)
                            {

                                // Values of theta [K] at upper and lower adjacent grid points.

                                double T_upper_K = inputGrids.at(0)->getValue(n, j, i + 1);
                                double p_upper_hPa = inputGrids.at(0)->getPressure(n, j, i + 1);
                                double theta_upper_K = potTemp(T_upper_K, p_upper_hPa);


                                double T_lower_K = inputGrids.at(6)->getValue(0, j, i + 1);
                                double p_lower_hPa = inputGrids.at(7)->getValue(0, j, i + 1) / 100.;
                                double theta_lower_K = potTemp(T_lower_K, p_lower_hPa);

                                // Interpolated value of theta [K].
                                thetaX_K = linearInterpolation1D(Z001_lower_m, Z001_upper_m,
                                                                 theta_lower_K, theta_upper_K,
                                                                 Z000_m);

                                break;
                            }

                            // Surface level is not involved.

                            else
                            {
                                // Values of theta [K] at upper and lower adjacent grid points.

                                double T_upper_K = inputGrids.at(0)->getValue(n, j, i + 1);
                                double p_upper_hPa = inputGrids.at(0)->getPressure(n, j, i + 1);
                                double theta_upper_K = potTemp(T_upper_K, p_upper_hPa);


                                double T_lower_K = inputGrids.at(0)->getValue(n + 1, j, i + 1);
                                double p_lower_hPa = inputGrids.at(0)->getPressure(n + 1, j, i + 1);
                                double theta_lower_K = potTemp(T_lower_K, p_lower_hPa);

                                // Interpolated value of theta [K].
                                thetaX_K = linearInterpolation1D(Z001_lower_m, Z001_upper_m,
                                                                 theta_lower_K, theta_upper_K,
                                                                 Z000_m);

                                break;
                            }

                        }

                        // Z000 is not enclosed between these levels, go one level up.

                        else
                        {
                            geopot001_lower_J = geopot001_upper_J;
                            Z001_lower_m = geopotToZ(geopot001_lower_J);

                            double virtualTemp_av_K = 0.;
                            double dPhi_J = 0.;


                            virtualTemp_av_K = ( virtualTempFromSpecHum(inputGrids.at(0)->getValue(n, j, i + 1),
                                                                        inputGrids.at(1)->getValue(n, j, i + 1))
                                               + virtualTempFromSpecHum(inputGrids.at(0)->getValue(n - 1, j, i + 1),
                                                                        inputGrids.at(1)->getValue(n - 1, j, i + 1)) ) / 2.;

                            dPhi_J = geopotThickness(virtualTemp_av_K,
                                                     inputGrids.at(0)->getPressure(n - 1, j, i + 1),
                                                     inputGrids.at(0)->getPressure(n, j, i + 1));


                            geopot001_upper_J += dPhi_J;
                            Z001_upper_m = geopotToZ(geopot001_upper_J);
                        }

                }

                // Theta on the constant geopotential height surface has been found.
                // Consider theta on a transformed Cartesian-like coordinates transformed from model levels
                // The vertical components of the tangent vectors are determined
                // from the two requirements stated above.

                double dZ_x_m = dZ_m * (theta000_K - thetaX_K) / (theta100_K - theta000_K) ;//Checked

                double dZ_y_m = dZ_x_m * (thetaY_K - theta000_K) / (thetaX_K - theta000_K);


            // From the current grid point, the tangent vectors now point to locations only
            // vertically displaced from the x- or y-neighbour, respectively.
            // The next step is to retrieve the wind components at these locations to compute
            // relative vorticity.

            // Find levels whose Z values enclose the geopotential height values
            // of the locations mentioned above and interpolate u and v to these values,
            // respectively.
            // The geopotential heights to interpolate are found by adding the tangent vectors' Z-component
            // to the current grid points' geopotential height.
            // The interpolation scheme is the same as for theta.

                    // x-coordinate
                    double vdx = 0.; // Interpolated value of v [m/s].
                    double Z_x_m = Z000_m + dZ_x_m; // Interpolation value of Z.

                    // Surface layer geopotential already computed.

                    // Lower neighbour.

                    double geopot001_J_lower = geopotSfc001_J;
                    double Zx_m_lower = geopotToZ(geopot001_J_lower);

                    // Upper neighbour.

                    double geopot001_J_upper = geopot001_J_lower + dPhiSfcLayer001_J;
                    double Zx_m_upper = geopotToZ(geopot001_J_upper);


                    for (n = derivedGrid->getNumLevels() - 1; n > 1; n--)
                    {

                            if ((Zx_m_lower <= Z_x_m) && (Z_x_m < Zx_m_upper))
                            {

                                // Surface level is involved.

                                if (n == derivedGrid->getNumLevels() - 1)
                                {
                                    // Values of v [m/s] at upper and lower adjacent grid points.
                                    double v_top = inputGrids.at(3)->getValue(inputGrids.at(3)->getNumLevels() - 1, j, i + 1);
                                    double v_bot = inputGrids.at(9)->getValue(0, j, i + 1);

                                    // Interpolated value of v.
                                    vdx = v_bot + (v_top - v_bot) * (Z_x_m - Zx_m_lower) / (Zx_m_upper - Zx_m_lower);

                                    break;
                                }

                                // Surface level is not involved.

                                else
                                {
                                    // Values of v [m/s] at upper and lower adjacent grid points.
                                    double v_top = inputGrids.at(3)->getValue(n, j, i + 1);
                                    double v_bot = inputGrids.at(3)->getValue(n + 1, j, i + 1);

                                    // Interpolated value of v.
                                    vdx = v_bot + (v_top - v_bot) * (Z_x_m - Zx_m_lower) / (Zx_m_upper - Zx_m_lower);

                                    break;
                                }

                            }

                            // Z_x is not enclosed between these levels, go one level up.

                            else
                            {
                                geopot001_J_lower = geopot001_J_upper;
                                Zx_m_lower = geopotToZ(geopot001_J_lower);

                                double virtualTemp_av_K = 0.;
                                double dPhi_J = 0.;

                                virtualTemp_av_K = ( virtualTempFromSpecHum(inputGrids.at(0)->getValue(n, j, i + 1),
                                                                            inputGrids.at(1)->getValue(n, j, i + 1))
                                                   + virtualTempFromSpecHum(inputGrids.at(0)->getValue(n - 1, j, i + 1),
                                                                            inputGrids.at(1)->getValue(n - 1, j, i + 1)) ) / 2.;

                                dPhi_J = geopotThickness(virtualTemp_av_K,
                                                         inputGrids.at(0)->getPressure(n - 1, j, i + 1),
                                                         inputGrids.at(0)->getPressure(n, j, i + 1));


                                geopot001_J_upper += dPhi_J;
                                Zx_m_upper = geopotToZ(geopot001_J_upper);
                            }

                    }

                    // y-coordinate

                    // (3c) Reuse geopotential height of current grid point and add Z component
                    // of y-Z-plane tangent vector.

                    double udy = 0.; // Interpolated value of u [m/s].
                    double Z_y_m = Z000_m + dZ_y_m; // Interpolation value of Z.


                    // Surface layer geopotential already computed.

                    // Lower neighbour.

                    double geopot010_J_lower = geopotSfc010_J;
                    double Zy_lower_m = geopotToZ(geopot010_J_lower);

                    // Upper neighbour.

                    double geopot010_J_upper = geopot010_J_lower + dPhiSfcLayer010_J;
                    double Zy_upper_m = geopotToZ(geopot010_J_upper);

                    for (n = derivedGrid->getNumLevels() - 1; n > 1; n--)
                    {

                            if((Zy_lower_m <= Z_y_m) && (Z_y_m < Zy_upper_m))
                            {

                                // Surface level is involved.

                                if (n == derivedGrid->getNumLevels() - 1)
                                {
                                    // Values of u [m/s] at upper and lower adjacent grid points.
                                    double u_top = inputGrids.at(2)->getValue(inputGrids.at(2)->getNumLevels() - 1, j + 1, i);
                                    double u_bot = inputGrids.at(8)->getValue(0, j + 1, i);

                                    // Interpolated value of u [m/s].
                                    udy = u_bot + (u_top - u_bot) * (Z_y_m - Zy_lower_m) / (Zy_upper_m - Zy_lower_m);

                                    break;
                                }

                                // Surface level is not involved.

                                else
                                {
                                    // Values of u [m/s] at upper and lower adjacent grid points.
                                    double u_top = inputGrids.at(2)->getValue(n, j + 1, i);
                                    double u_bot = inputGrids.at(2)->getValue(n + 1, j + 1, i);

                                    // Interpolated value of u [m/s].
                                    udy = u_bot + (u_top - u_bot) * (Z_y_m - Zy_lower_m) / (Zy_upper_m - Zy_lower_m);

                                    break;
                                }

                            }

                            // Z_y is not enclosed between these levels, go one level up.

                            else
                            {
                                geopot010_J_lower = geopot010_J_upper;
                                Zy_lower_m = geopotToZ(geopot010_J_lower);

                                double virtualTemp_av_K = 0.;
                                double dPhi_J = 0.;

                                virtualTemp_av_K = ( virtualTempFromSpecHum(inputGrids.at(0)->getValue(n, j + 1, i),
                                                                            inputGrids.at(1)->getValue(n, j + 1, i))
                                                   + virtualTempFromSpecHum(inputGrids.at(0)->getValue(n - 1, j + 1, i),
                                                                            inputGrids.at(1)->getValue(n - 1, j + 1, i)) ) / 2.;

                                dPhi_J = geopotThickness(virtualTemp_av_K,
                                                         inputGrids.at(0)->getPressure(n - 1, j + 1, i),
                                                         inputGrids.at(0)->getPressure(n, j + 1, i));


                                geopot010_J_upper += dPhi_J;
                                Zy_upper_m = geopotToZ(geopot010_J_upper);
                            }

                    }

                // Compute geometrical length of tangent vectors.

                double radius_m = MetConstants::EARTH_RADIUS_km * 1000.;

                double dx_m = gcDistance_deg(inputGrids.at(0)->getEastInterfaceLon(i),
                                             inputGrids.at(0)->getNorthInterfaceLat(j),
                                             inputGrids.at(0)->getEastInterfaceLon(i + 1),
                                             inputGrids.at(0)->getNorthInterfaceLat(j),
                                             radius_m);

                double dy_m = gcDistance_deg(inputGrids.at(0)->getEastInterfaceLon(i),
                                             inputGrids.at(0)->getNorthInterfaceLat(j),
                                             inputGrids.at(0)->getEastInterfaceLon(i),
                                             inputGrids.at(0)->getNorthInterfaceLat(j + 1),
                                             radius_m);

                double dx_Z_m = vecLengthZ(dx_m, 0., dZ_x_m);//geometric length of dx_theta

                double dy_Z_m = vecLengthZ(0., dy_m, dZ_y_m);//geometric length of dy_theta

                // Compute wind differences [m/s]

                double du = udy - inputGrids.at(2)->getValue(k, j, i);
                double dv = vdx - inputGrids.at(3)->getValue(k, j, i);

                // Compute isentropic relative vorticity [s^-1].
                double vo_rel = (dv / dx_Z_m) - (du / dy_Z_m);

                // Compute values for static stability [K/Pa].

                double dTheta = theta100_K - theta000_K;
                double dp = 100. * (p100_hPa - p000_hPa);

                // Compute planetary vorticity f = 2 * w_0 * sin(2 * PI * lat / 360) [s^-1].

                double lat000_deg = inputGrids.at(0)->getNorthInterfaceLat(j);
                double f = planetaryVorticity(lat000_deg);

                // Compute PV[pvu].

                double factor = MetConstants::PVU_FACTOR;
                double g = MetConstants::GRAVITY_ACCELERATION;
                double staticStability = dTheta / dp;
                double PV_pvu = -factor * g * (vo_rel + f) * staticStability;

                derivedGrid->setValue(k, j, i, PV_pvu);
            }
        } //end pragma
    }


    // OLD ISENTROPIC POTENTIAL VORTICITY
    else if ( (derivedVarName == "isentropic PV (simplified) (an)")
              || (derivedVarName == "isentropic PV (simplified) (fc)")
              || (derivedVarName == "isentropic PV (simplified)(ens)") )
    {
        unsigned int k, j, i;
        #pragma omp parallel
        {
            #pragma omp for private(j, i)
            for (k = 1; k < derivedGrid->getNumLevels() - 1; k++)
                for (j = 0; j < derivedGrid->getNumLats() - 1; j++)
                    for (i = 0; i < derivedGrid->getNumLons() - 1; i++)
            {
                // 0 t, 1 q, 2 u, 3 v
                // Raphido: Why is there only a method to get north interface lat?

                float PV_pvu = PVFromT_P_U_V_lat(inputGrids.at(2)->getValue(k, j, i),
                                                 inputGrids.at(3)->getValue(k, j, i),
                                                 inputGrids.at(0)->getValue(k, j, i),
                                                 inputGrids.at(0)->getPressure(k, j, i),
                                                 inputGrids.at(0)->getEastInterfaceLon(i),
                                                 inputGrids.at(0)->getNorthInterfaceLat(j),

                                                 inputGrids.at(2)->getValue(k, j+1, i),
                                                 inputGrids.at(0)->getValue(k, j+1, i),
                                                 inputGrids.at(0)->getPressure(k, j+1, i),
                                                 inputGrids.at(0)->getEastInterfaceLon(i),
                                                 inputGrids.at(0)->getNorthInterfaceLat(j+1),
                                                 inputGrids.at(1)->getValue(k, j+1, i),

                                                 inputGrids.at(2)->getValue(k+1, j+1, i),
                                                 inputGrids.at(0)->getValue(k+1, j+1, i),
                                                 inputGrids.at(0)->getPressure(k+1, j+1, i),
                                                 inputGrids.at(1)->getValue(k+1, j+1, i),

                                                 inputGrids.at(2)->getValue(k-1, j+1, i),
                                                 inputGrids.at(0)->getValue(k-1, j+1, i),
                                                 inputGrids.at(0)->getPressure(k-1, j+1, i),
                                                 inputGrids.at(1)->getValue(k-1, j+1, i),

                                                 inputGrids.at(3)->getValue(k, j, i+1),
                                                 inputGrids.at(0)->getValue(k, j, i+1),
                                                 inputGrids.at(0)->getPressure(k, j, i+1),
                                                 inputGrids.at(0)->getEastInterfaceLon(i+1),
                                                 inputGrids.at(0)->getNorthInterfaceLat(j),
                                                 inputGrids.at(1)->getValue(k, j, i+1),

                                                 inputGrids.at(3)->getValue(k+1, j, i+1),
                                                 inputGrids.at(0)->getValue(k+1, j, i+1),
                                                 inputGrids.at(0)->getPressure(k+1, j, i+1),
                                                 inputGrids.at(1)->getValue(k+1, j, i+1),

                                                 inputGrids.at(3)->getValue(k-1, j, i+1),
                                                 inputGrids.at(0)->getValue(k-1, j, i+1),
                                                 inputGrids.at(0)->getPressure(k-1, j, i+1),
                                                 inputGrids.at(1)->getValue(k-1, j, i+1),

                                                 inputGrids.at(0)->getValue(k+1, j, i),
                                                 inputGrids.at(0)->getPressure(k+1, j, i)
                                                 );

                derivedGrid->setValue(k, j, i, PV_pvu);
    } //end pragma
        }
            }


    // GEOPOTENTIAL HEIGHT
    else if ( (derivedVarName == "geopotential height (an)")
              || (derivedVarName == "geopotential height (fc)")
              || (derivedVarName == "geopotential height (test)")
              || (derivedVarName == "geopotential height (ens)") )
    {
        unsigned int k, j, i;
        #pragma omp parallel
        {
            #pragma omp for private(j, i)
            for (k = 0; k < derivedGrid->getNumLevels() - 1; k++)
                for (j = 0; j < derivedGrid->getNumLats(); j++)
                    for (i = 0; i < derivedGrid->getNumLons(); i++)
            {

                float geopotSfc_J = inputGrids.at(2)->getValue(0, j, i);
                float pSfc_hPa = inputGrids.at(5)->getValue(0, j, i) / 100.;

                // Assuming temperature ranging from -45 to +60 degrees Celsius

                // Virtual temperature

                float virtualTempSfc_K = virtualTempFromDewPoint(inputGrids.at(3)->getValue(0, j, i),
                                                                  pSfc_hPa,
                                                                  inputGrids.at(4)->getValue(0, j, i));

                float virtualTempFirstLvl_K = virtualTempFromSpecHum(inputGrids.at(0)->getValue(inputGrids.at(0)->getNumLevels() - 1, j, i),
                                                                     inputGrids.at(1)->getValue(inputGrids.at(1)->getNumLevels() - 1, j, i));

                float meanVirtualTemp_K = (virtualTempSfc_K + virtualTempFirstLvl_K) / 2.;

                // Surface layer geopotential thickness
                float dPhiSfcLayer = geopotThickness(meanVirtualTemp_K,
                                                      inputGrids.at(0)->getPressure(inputGrids.at(0)->getNumLevels() - 1, j, i),
                                                      pSfc_hPa);

                float geopot_J = 0.;

                float virtualTemp_av_K = 0.;
                float dPhi_J = 0.;

                // For each point, loop from surface pressure level to current pressure level (k).
                for (unsigned int l = derivedGrid->getNumLevels() - 2; l > k-1; l--)
                {

                    // Compute average virtual temperature between current pressure level
                    // and lower level.
                    virtualTemp_av_K = ( virtualTempFromSpecHum( inputGrids.at(0)->getValue(l, j, i),
                                                                 inputGrids.at(1)->getValue(l, j, i) )
                                       + virtualTempFromSpecHum( inputGrids.at(0)->getValue(l+1, j, i),
                                                                 inputGrids.at(1)->getValue(l+1, j, i) ) ) / 2.;

                    // Compute the current thickness.
                    dPhi_J = geopotThickness(virtualTemp_av_K,
                                             inputGrids.at(0)->getPressure(l, j, i),
                                             inputGrids.at(0)->getPressure(l+1, j, i));

                    // Sum up the differentials.
                    geopot_J += dPhi_J;
                }

                // Add surface layer geopotential thickness
                geopot_J += dPhiSfcLayer;

                // Add surface geopotential
                geopot_J += geopotSfc_J;

                // Convert to geopotential height by dividing through g_0.
                float geopotHeight_gpm = geopotToZ(geopot_J);

                derivedGrid->setValue(k, j, i, geopotHeight_gpm);
            }
        } // end pragma
    }


    // 1000 hPa GEOPOTENTIAL HEIGHT
    else if ( (derivedVarName == "1000hPa Z (an)")
              || (derivedVarName == "1000hPa Z (fc)")
              || (derivedVarName == "1000hPa Z (ens)") )
    {
        unsigned int j, i;
        #pragma omp parallel
        {
            #pragma omp for private(i)
            for (j = 0; j < derivedGrid->getNumLats(); j++)
                for (i = 0; i < derivedGrid->getNumLons(); i++)
            {

                float refLvlhPaZ_gpm = sfcTo1000hPaGeopot(inputGrids.at(0)->getValue(0, j, i),
                                                          inputGrids.at(1)->getValue(0, j, i),
                                                          inputGrids.at(2)->getValue(0, j, i));

                derivedGrid->setValue(0, j, i, refLvlhPaZ_gpm);
            }
        }
    }


    // Release input fields.
    foreach (MStructuredGrid *inputGrid, inputGrids)
        inputSource->releaseData(inputGrid);

    return derivedGrid;

}


MTask* MDerivedMetVarsDataSource::createTaskGraph(MDataRequest request)
{
    assert(inputSource != nullptr);

    MTask *task = new MTask(request, this);

    MDataRequestHelper rh(request);
    QString derivedVarName = rh.value("VARIABLE");
    rh.removeAll(locallyRequiredKeys());

    foreach (QString requiredVar, requiredInputVariablesList[derivedVarName])
    {
        QStringList requiredVarDef = requiredVar.split("/");
        QString varName = requiredVarDef.at(0);
        MVerticalLevelType levelType = MStructuredGrid::verticalLevelTypeFromConfigString(requiredVarDef.at(1));
        rh.insert("VARIABLE", varName);
        rh.insert("LEVELTYPE", levelType);
        task->addParent(inputSource->getTaskGraph(rh.request()));
    }

    return task;
}


QList<MVerticalLevelType> MDerivedMetVarsDataSource::availableLevelTypes()
{
    assert(inputSource != nullptr);
    return inputSource->availableLevelTypes();
}


QStringList MDerivedMetVarsDataSource::availableVariables(
        MVerticalLevelType levelType)
{
    assert(inputSource != nullptr);

    QStringList availableVars;

    // For each variable that can be derived, check if required input variables
    // are available. If yes, add the derived variable to the list of available
    // variables.
    foreach (QString derivedVarName, requiredInputVariablesList.keys())
    {
        // If one of the required variables is not available from the input
        // source, skip.
        bool requiredInputVarsAvailable = true;
        foreach (QString requiredVar, requiredInputVariablesList[derivedVarName])
        {
            // THIS IS JUST A TEMPORARY SOLUTION!!!
            QStringList requiredVarDef = requiredVar.split("/");
            QString varName = requiredVarDef.at(0);
            levelType = MStructuredGrid::verticalLevelTypeFromConfigString(requiredVarDef.at(1));

            if ( ( !inputSource->availableLevelTypes().contains(levelType) )
                 || ( !inputSource->availableVariables(levelType).contains(varName) ) )
            {
                requiredInputVarsAvailable = false;
                break;
            }
        }

        if (requiredInputVarsAvailable)
            availableVars << derivedVarName;
    }

    return availableVars;
}


QSet<unsigned int> MDerivedMetVarsDataSource::availableEnsembleMembers(
        MVerticalLevelType levelType, const QString& variableName)
{
    assert(inputSource != nullptr);

    QSet<unsigned int> members;
    foreach (QString inputVar, requiredInputVariablesList[variableName])
    {
        // THIS IS JUST A TEMPORARY SOLUTION!!!
        QStringList inputVarDef = inputVar.split("/");
        QString varName = inputVarDef.at(0);
        levelType = MStructuredGrid::verticalLevelTypeFromConfigString(inputVarDef.at(1));

        if (members.isEmpty())
        {
            members = inputSource->availableEnsembleMembers(
                        levelType, varName);
        }
        else
        {
            members = members.intersect(inputSource->availableEnsembleMembers(
                                            levelType, varName));
        }
    }

    return members;
}


QList<QDateTime> MDerivedMetVarsDataSource::availableInitTimes(
        MVerticalLevelType levelType, const QString& variableName)
{
    assert(inputSource != nullptr);

//NOTE: Qt4.8 does not yet support QList<QDateTime> with the intersect()
// method (qHash doesn't support QDateTime types).
//TODO (mr, 2016May17) -- Change this to QSet when switching to Qt 5.X.
    QList<QDateTime> times;
    foreach (QString inputVar, requiredInputVariablesList[variableName])
    {
        // THIS IS JUST A TEMPORARY SOLUTION!!!
        QStringList inputVarDef = inputVar.split("/");
        QString varName = inputVarDef.at(0);
        levelType = MStructuredGrid::verticalLevelTypeFromConfigString(inputVarDef.at(1));

        if (times.isEmpty())
        {
            times = inputSource->availableInitTimes(levelType, varName);
        }
        else
        {
            QList<QDateTime> inputTimes = inputSource->availableInitTimes(
                        levelType, varName);

            foreach (QDateTime dt, times)
                if ( !inputTimes.contains(dt) ) times.removeOne(dt);
        }
    }

    return times;
}


QList<QDateTime> MDerivedMetVarsDataSource::availableValidTimes(
        MVerticalLevelType levelType,
        const QString& variableName, const QDateTime& initTime)
{
    assert(inputSource != nullptr);

    QList<QDateTime> times;
    foreach (QString inputVar, requiredInputVariablesList[variableName])
    {
        // THIS IS JUST A TEMPORARY SOLUTION!!!
        QStringList inputVarDef = inputVar.split("/");
        QString varName = inputVarDef.at(0);
        levelType = MStructuredGrid::verticalLevelTypeFromConfigString(inputVarDef.at(1));

        if (times.isEmpty())
        {
            times = inputSource->availableValidTimes(levelType, varName, initTime);
        }
        else
        {
            QList<QDateTime> inputTimes = inputSource->availableValidTimes(
                        levelType, varName, initTime);

            foreach (QDateTime dt, times)
                if ( !inputTimes.contains(dt) ) times.removeOne(dt);
        }
    }

    return times;
}



/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

const QStringList MDerivedMetVarsDataSource::locallyRequiredKeys()
{
    return (QStringList() << "VARIABLE");
}

} // namespace Met3D

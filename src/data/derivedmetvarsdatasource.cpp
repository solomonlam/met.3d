/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2018 Marc Rautenhaus
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "derivedmetvarsdatasource.h"

// standard library imports
#include <iostream>
#include "assert.h"

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "util/metroutines.h"
#include "util/metroutines_experimental.h"
#include "util/mexception.h"

using namespace std;

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MDerivedDataFieldProcessor::MDerivedDataFieldProcessor(
        QString standardName, QStringList requiredInputVariables)
    : standardName(standardName),
      requiredInputVariables(requiredInputVariables)
{
}


MDerivedMetVarsDataSource::MDerivedMetVarsDataSource()
    : MProcessingWeatherPredictionDataSource(),
      inputSource(nullptr)
{
    // Register data field processors.
    // ===============================
//!Todo (mr, 11Mar2018). This could possibly be moved out of this constructor
//! and be done outside of the class as a configuration/plug-in mechanism.

    registerDerivedDataFieldProcessor(new MHorizontalWindSpeedProcessor());
    registerDerivedDataFieldProcessor(new MPotentialTemperatureProcessor());
    registerDerivedDataFieldProcessor(new MEquivalentPotentialTemperatureProcessor());
    registerDerivedDataFieldProcessor(new MGeopotentialHeightProcessor());
    registerDerivedDataFieldProcessor(new MGeopotentialHeightFromGeopotentialProcessor());

    registerDerivedDataFieldProcessor(
                new MMagnitudeOfVerticallyIntegratedMoistureFluxProcessor(
                    "HYBRID_SIGMA_PRESSURE_3D"));
//!TODO (mr, 13Mar2018) -- there needs to be a more elegant way to handle
//! cases in which the returned data field is of different type than all
//! required input fields. The current solution appends the input level type
//! to the variable name, which is not very elegant...
    registerDerivedDataFieldProcessor(
                new MMagnitudeOfVerticallyIntegratedMoistureFluxProcessor(
                    "PRESSURE_LEVELS_3D"));
    registerDerivedDataFieldProcessor(
                new MMagnitudeOfVerticallyIntegratedMoistureFluxProcessor(
                    "AUXILIARY_PRESSURE_3D"));

    registerDerivedDataFieldProcessor(new MTHourlyTotalPrecipitationProcessor(1));
    registerDerivedDataFieldProcessor(new MTHourlyTotalPrecipitationProcessor(3));
    registerDerivedDataFieldProcessor(new MTHourlyTotalPrecipitationProcessor(6));
    registerDerivedDataFieldProcessor(new MTHourlyTotalPrecipitationProcessor(12));
    registerDerivedDataFieldProcessor(new MTHourlyTotalPrecipitationProcessor(24));

    // Register experimental data field processors.
    // ============================================

    // ... <add registration commands here> ...
    registerDerivedDataFieldProcessor(new MIsentropicPotentialVorticityProcessor());
}


MDerivedMetVarsDataSource::~MDerivedMetVarsDataSource()
{
    foreach (MDerivedDataFieldProcessor* p, registeredDerivedDataProcessors)
    {
        delete p;
    }
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MDerivedMetVarsDataSource::setInputSource(MWeatherPredictionDataSource* s)
{
    inputSource = s;
    registerInputSource(inputSource);
    //    enablePassThrough(s);
}


void MDerivedMetVarsDataSource::setInputVariable(
        QString standardName, QString inputVariableName)
{
    variableStandardNameToInputNameMapping[standardName] = inputVariableName;
}


void MDerivedMetVarsDataSource::registerDerivedDataFieldProcessor(
        MDerivedDataFieldProcessor *processor)
{
    if (processor != nullptr)
    {
        registeredDerivedDataProcessors.insert(
                    processor->getStandardName(), processor);

        requiredInputVariablesList[processor->getStandardName()] =
                processor->getRequiredInputVariables();
    }
}


MStructuredGrid* MDerivedMetVarsDataSource::produceData(MDataRequest request)
{
#ifdef ENABLE_MET3D_STOPWATCH
    MStopwatch stopwatch;
#endif

    assert(inputSource != nullptr);

    // Parse request.
    MDataRequestHelper rh(request);
    QString derivedVarName = rh.value("VARIABLE");
    MVerticalLevelType levelType = MVerticalLevelType(rh.intValue("LEVELTYPE"));
    QDateTime initTime  = rh.timeValue("INIT_TIME");
    QDateTime validTime = rh.timeValue("VALID_TIME");
    rh.removeAll(locallyRequiredKeys()); // removes "VARIABLE"

    // Get input fields.
    QList<MStructuredGrid*> inputGrids;
    foreach (QString requiredVarStdName,
             requiredInputVariablesList[derivedVarName])
    {
        // Handle enforced level types (cf. updateStdNameAndLevelType()).
        MVerticalLevelType ltype = levelType;
        bool argsChanged = updateStdNameAndArguments(
                    &requiredVarStdName, &ltype, &initTime, &validTime);

        QString inputVarName = getInputVariableNameFromStdName(
                    requiredVarStdName);
        rh.insert("VARIABLE", inputVarName);
        rh.insert("LEVELTYPE", ltype); // update if changed
        rh.insert("INIT_TIME", initTime); // update if changed
        rh.insert("VALID_TIME", validTime); // update if changed

        // Check if if times have been changed the requested data fields are
        // available.
        bool everythingAvailable = true;
        if ( argsChanged && !inputSource->availableInitTimes(
                 ltype, inputVarName).contains(initTime) )
        {
            everythingAvailable = false;
        }
        else if ( argsChanged && !inputSource->availableValidTimes(
                      ltype, inputVarName, initTime).contains(validTime) )
        {
            everythingAvailable = false;
        }

        if (everythingAvailable)
        {
            inputGrids << inputSource->getData(rh.request());
        }
        else
        {
            // If an input request fails (e.g. if the field 6-h earlier is
            // also requested but not available), add a nullptr so that
            // the number of entries in the list is consistent. It is the
            // responsibility of the processor module to check...
            inputGrids << nullptr;
        }
    }

    // Initialize result grid.
    MStructuredGrid *derivedGrid = nullptr;
    if ( !inputGrids.isEmpty() && inputGrids.at(0) != nullptr )
    {
        derivedGrid = createAndInitializeResultGrid(inputGrids.at(0));
        derivedGrid->setMetaData(inputGrids.at(0)->getInitTime(),
                                 inputGrids.at(0)->getValidTime(),
                                 derivedVarName,
                                 inputGrids.at(0)->getEnsembleMember());
    }

    // Compute derived grid.
    if ((derivedGrid != nullptr) &&
            registeredDerivedDataProcessors.contains(derivedVarName))
    {
        registeredDerivedDataProcessors[derivedVarName]->compute(
                    inputGrids, derivedGrid);
    }

    // Release input fields.
    foreach (MStructuredGrid *inputGrid, inputGrids)
    {
        if (inputGrid == nullptr) continue;
        inputSource->releaseData(inputGrid);
    }

#ifdef ENABLE_MET3D_STOPWATCH
    stopwatch.split();
    LOG4CPLUS_DEBUG(mlog, "computed derived data field "
                    << derivedVarName.toStdString()
                    << " in " << stopwatch.getLastSplitTime(MStopwatch::SECONDS)
                    << " seconds.\n" << flush);
#endif

    return derivedGrid;
}


MTask* MDerivedMetVarsDataSource::createTaskGraph(MDataRequest request)
{
    assert(inputSource != nullptr);

    MTask *task = new MTask(request, this);

    MDataRequestHelper rh(request);
    QString derivedVarName = rh.value("VARIABLE");
    MVerticalLevelType levelType = MVerticalLevelType(rh.intValue("LEVELTYPE"));
    QDateTime initTime  = rh.timeValue("INIT_TIME");
    QDateTime validTime = rh.timeValue("VALID_TIME");
    rh.removeAll(locallyRequiredKeys()); // removes "VARIABLE"

    foreach (QString requiredVarStdName,
             requiredInputVariablesList[derivedVarName])
    {
        // Handle enforced level types (cf. updateStdNameAndLevelType()).
        MVerticalLevelType ltype = levelType;
        bool argsChanged = updateStdNameAndArguments(
                    &requiredVarStdName, &ltype, &initTime, &validTime);

        QString inputVarName = getInputVariableNameFromStdName(
                    requiredVarStdName);
        rh.insert("VARIABLE", inputVarName);
        rh.insert("LEVELTYPE", ltype); // update if changed
        rh.insert("INIT_TIME", initTime); // update if changed
        rh.insert("VALID_TIME", validTime); // update if changed

        // Check if if times have been changed the requested data fields are
        // available.
        bool everythingAvailable = true;
        if ( argsChanged && !inputSource->availableInitTimes(
                 ltype, inputVarName).contains(initTime) )
        {
            everythingAvailable = false;
        }
        else if ( argsChanged && !inputSource->availableValidTimes(
                      ltype, inputVarName, initTime).contains(validTime) )
        {
            everythingAvailable = false;
        }

        if (everythingAvailable)
        {
            task->addParent(inputSource->getTaskGraph(rh.request()));
        }
    }

    return task;
}


QList<MVerticalLevelType> MDerivedMetVarsDataSource::availableLevelTypes()
{
    assert(inputSource != nullptr);
    return inputSource->availableLevelTypes();
}


QStringList MDerivedMetVarsDataSource::availableVariables(
        MVerticalLevelType levelType)
{
    assert(inputSource != nullptr);

    QStringList availableVars;

    // For each variable that can be derived, check if required input variables
    // are available. If yes, add the derived variable to the list of available
    // variables.
    foreach (QString derivedVarName, requiredInputVariablesList.keys())
    {
        bool requiredInputVarsAvailable = true;

        foreach (QString requiredVarStdName,
                 requiredInputVariablesList[derivedVarName])
        {
            // Handle enforced level types.
            MVerticalLevelType ltype = levelType;
            updateStdNameAndArguments(&requiredVarStdName, &ltype);

            // If one of the required variables is not available from the
            // input source, skip.            
            if ( !inputSource->availableLevelTypes().contains(ltype) ||
                 !inputSource->availableVariables(ltype).contains(
                     getInputVariableNameFromStdName(requiredVarStdName)) )
            {
                requiredInputVarsAvailable = false;
                break;
            }
        }

        if (requiredInputVarsAvailable)
            availableVars << derivedVarName;
    }

    return availableVars;
}


QSet<unsigned int> MDerivedMetVarsDataSource::availableEnsembleMembers(
        MVerticalLevelType levelType, const QString& variableName)
{
    assert(inputSource != nullptr);

    QSet<unsigned int> members;
    foreach (QString inputVarStdName, requiredInputVariablesList[variableName])
    {
        // Handle enforced level types.
        MVerticalLevelType ltype = levelType;
        updateStdNameAndArguments(&inputVarStdName, &ltype);

        if (members.isEmpty())
        {
            members = inputSource->availableEnsembleMembers(
                        ltype,
                        getInputVariableNameFromStdName(inputVarStdName));
        }
        else
        {
            members = members.intersect(
                        inputSource->availableEnsembleMembers(
                            ltype,
                            getInputVariableNameFromStdName(inputVarStdName)));
        }
    }

    return members;
}


QList<QDateTime> MDerivedMetVarsDataSource::availableInitTimes(
        MVerticalLevelType levelType, const QString& variableName)
{
    assert(inputSource != nullptr);

//NOTE: Qt4.8 does not yet support QList<QDateTime> with the intersect()
// method (qHash doesn't support QDateTime types).
//TODO (mr, 2016May17) -- Change this to QSet when switching to Qt 5.X.
    QList<QDateTime> times;
    foreach (QString inputVarStdName, requiredInputVariablesList[variableName])
    {
        // Handle enforced level types.
        MVerticalLevelType ltype = levelType;
        updateStdNameAndArguments(&inputVarStdName, &ltype);

        if (times.isEmpty())
        {
            times = inputSource->availableInitTimes(
                        ltype,
                        getInputVariableNameFromStdName(inputVarStdName));
        }
        else
        {
            QList<QDateTime> inputTimes = inputSource->availableInitTimes(
                        ltype,
                        getInputVariableNameFromStdName(inputVarStdName));

            foreach (QDateTime dt, times)
            {
                if ( !inputTimes.contains(dt) )
                {
                    times.removeOne(dt);
                }
            }
        }
    }

    return times;
}


QList<QDateTime> MDerivedMetVarsDataSource::availableValidTimes(
        MVerticalLevelType levelType,
        const QString& variableName, const QDateTime& initTime)
{
    assert(inputSource != nullptr);

    QList<QDateTime> times;
    foreach (QString inputVarStdName, requiredInputVariablesList[variableName])
    {
        // Handle enforced level types.
        MVerticalLevelType ltype = levelType;
        updateStdNameAndArguments(&inputVarStdName, &ltype);

        if (times.isEmpty())
        {
            times = inputSource->availableValidTimes(
                        ltype,
                        getInputVariableNameFromStdName(inputVarStdName),
                        initTime);
        }
        else
        {
            QList<QDateTime> inputTimes = inputSource->availableValidTimes(
                        ltype,
                        getInputVariableNameFromStdName(inputVarStdName),
                        initTime);

            foreach (QDateTime dt, times)
            {
                if ( !inputTimes.contains(dt) )
                {
                    times.removeOne(dt);
                }
            }
        }
    }

    return times;
}


QString MDerivedMetVarsDataSource::variableLongName(
        MVerticalLevelType levelType,
        const QString& variableName)
{
    Q_UNUSED(levelType);

    QString longName = QString("%1, computed from ").arg(variableName);

    foreach (QString inputVarStdName, requiredInputVariablesList[variableName])
    {
        // Handle enforced level types.
        MVerticalLevelType ltype = levelType;
        updateStdNameAndArguments(&inputVarStdName, &ltype);

        longName += QString("%1/").arg(
                    getInputVariableNameFromStdName(inputVarStdName));
    }
    // Remove last "/" character.
    longName.resize(longName.length()-1);

    return longName;
}


QString MDerivedMetVarsDataSource::variableStandardName(
        MVerticalLevelType levelType,
        const QString& variableName)
{
    Q_UNUSED(levelType);

    // Special property of this data source: variable names equal CF standard
    // names.
    return variableName;
}


QString MDerivedMetVarsDataSource::variableUnits(
        MVerticalLevelType levelType,
        const QString& variableName)
{
    Q_UNUSED(levelType); Q_UNUSED(variableName);
    return QString();
}



/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

const QStringList MDerivedMetVarsDataSource::locallyRequiredKeys()
{
    return (QStringList() << "LEVELTYPE" << "VARIABLE" << "INIT_TIME"
            << "VALID_TIME");
}


QString MDerivedMetVarsDataSource::getInputVariableNameFromStdName(
        QString stdName)
{
    QString inputVariableName;
    if (variableStandardNameToInputNameMapping.contains(stdName))
    {
        inputVariableName = variableStandardNameToInputNameMapping[stdName];
    }
    return inputVariableName;
}


bool MDerivedMetVarsDataSource::updateStdNameAndArguments(
        QString *stdName, MVerticalLevelType *levelType,
        QDateTime *initTime, QDateTime *validTime)
{
    bool changedArguments = false;

    // Assume somthing like "surface_geopotential/SURFACE_2D" passed in stdName.
    // If only a variable name is passed (e.g., ""surface_geopotential"),
    // nothing is changed.
    QStringList definitionsList = stdName->split("/");
    if (definitionsList.size() >= 2)
    {
        *stdName = definitionsList.at(0);

        MVerticalLevelType newLevelType =
                MStructuredGrid::verticalLevelTypeFromConfigString(
                    definitionsList.at(1));

        // If a valid leveltype has been defined, update the passed variables.
        if (newLevelType != SIZE_LEVELTYPES)
        {
            *levelType = newLevelType;
            changedArguments = true;
        }
    }

    // Assume something like "lwe_thickness_of_precipitation_amount//-43200"
    // is passed. This will substract 43200 seconds = 12 hours from the
    // INIT_TIME.
    if (definitionsList.size() >= 3 && initTime != nullptr)
    {
        bool ok;
        int timeDifference_sec = definitionsList.at(2).toInt(&ok);
        if (ok)
        {
            *initTime = initTime->addSecs(timeDifference_sec);
            changedArguments = true;
        }
    }

    // Assume something like "lwe_thickness_of_precipitation_amount///-21600"
    // is passed. This will substract 21600 seconds = 6 hours from the
    // VALID_TIME.
    if (definitionsList.size() == 4 && validTime != nullptr)
    {
        bool ok;
        int timeDifference_sec = definitionsList.at(3).toInt(&ok);
        if (ok)
        {
            *validTime = validTime->addSecs(timeDifference_sec);
            changedArguments = true;
        }
    }

    return changedArguments;
}



/******************************************************************************
***                            DATA PROCESSORS                              ***
*******************************************************************************/

// Wind Speed
// ==========

MHorizontalWindSpeedProcessor::MHorizontalWindSpeedProcessor()
    : MDerivedDataFieldProcessor(
          "wind_speed",
          QStringList() << "eastward_wind" << "northward_wind")
{}


void MHorizontalWindSpeedProcessor::compute(
        QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "eastward_wind"
    // input 1 = "northward_wind"

    for (unsigned int n = 0; n < derivedGrid->getNumValues(); n++)
    {
        float u_ms = inputGrids.at(0)->getValue(n);
        float v_ms = inputGrids.at(1)->getValue(n);

        if (u_ms == M_MISSING_VALUE || v_ms == M_MISSING_VALUE)
        {
            derivedGrid->setValue(n, M_MISSING_VALUE);
        }
        else
        {
            float windspeed = windSpeed_ms(u_ms, v_ms);
            derivedGrid->setValue(n, windspeed);
        }
    }
}


MPotentialTemperatureProcessor::MPotentialTemperatureProcessor()
    : MDerivedDataFieldProcessor(
          "air_potential_temperature",
          QStringList() << "air_temperature")
{
}


// Potential temperature
// =====================

void MPotentialTemperatureProcessor::compute(
        QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "air_temperature"

    // Requires nested k/j/i loops to access pressure at grid point.
    for (unsigned int k = 0; k < derivedGrid->getNumLevels(); k++)
        for (unsigned int j = 0; j < derivedGrid->getNumLats(); j++)
            for (unsigned int i = 0; i < derivedGrid->getNumLons(); i++)
            {
                float T_K = inputGrids.at(0)->getValue(k, j, i);

                if (T_K == M_MISSING_VALUE)
                {
                    derivedGrid->setValue(k, j, i, M_MISSING_VALUE);
                }
                else
                {
                    float theta_K = potentialTemperature_K(
                                T_K,
                                inputGrids.at(0)->getPressure(k, j, i) * 100.);
                    derivedGrid->setValue(k, j, i, theta_K);
                }
            }
}


// Equivalent potential temperature
// ================================

MEquivalentPotentialTemperatureProcessor
::MEquivalentPotentialTemperatureProcessor()
    : MDerivedDataFieldProcessor(
          "equivalent_potential_temperature",
          QStringList() << "air_temperature" << "specific_humidity")
{
}


void MEquivalentPotentialTemperatureProcessor::compute(
        QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "air_temperature"
    // input 1 = "specific_humidity"

    // Requires nested k/j/i loops to access pressure at grid point.
    for (unsigned int k = 0; k < derivedGrid->getNumLevels(); k++)
        for (unsigned int j = 0; j < derivedGrid->getNumLats(); j++)
            for (unsigned int i = 0; i < derivedGrid->getNumLons(); i++)
            {
                float T_K = inputGrids.at(0)->getValue(k, j, i);
                float q_kgkg = inputGrids.at(1)->getValue(k, j, i);

                if (T_K == M_MISSING_VALUE || q_kgkg == M_MISSING_VALUE)
                {
                    derivedGrid->setValue(k, j, i, M_MISSING_VALUE);
                }
                else
                {
//!TODO (mr, 14Mar2018) -- possibly replace the Bolton equation by a more
//! recent formula. See Davies-Jones (MWR, 2009), "On Formulas for Equiv.
//! Potential Temperature".
                    float thetaE_K = equivalentPotentialTemperature_K_Bolton(
                                T_K,
                                inputGrids.at(0)->getPressure(k, j, i) * 100.,
                                q_kgkg);
                    derivedGrid->setValue(k, j, i, thetaE_K);
                }
            }
}


// Geopotential height
// ===================

MGeopotentialHeightProcessor::MGeopotentialHeightProcessor()
    : MDerivedDataFieldProcessor(
          "geopotential_height",
          QStringList() << "air_temperature" << "specific_humidity"
                        << "surface_geopotential/SURFACE_2D"
                        << "surface_air_pressure/SURFACE_2D"
                        << "surface_temperature/SURFACE_2D")
{
}


void MGeopotentialHeightProcessor::compute(
        QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "air_temperature"
    // input 1 = "specific_humidity"
    // input 2 = "surface_geopotential"
    // input 3 = "surface_air_pressure"
    // input 4 = "surface_temperature"

    MStructuredGrid *airTemperatureGrid = inputGrids.at(0);
    MStructuredGrid *specificHumidityGrid = inputGrids.at(1);

    // Cast surface grids to get access to 2D getValue() method.
    MRegularLonLatGrid *surfaceGeopotentialGrid =
            dynamic_cast<MRegularLonLatGrid*>(inputGrids.at(2));
    MRegularLonLatGrid *surfaceAirPressureGrid =
            dynamic_cast<MRegularLonLatGrid*>(inputGrids.at(3));
    MRegularLonLatGrid *surfaceTemperatureGrid =
            dynamic_cast<MRegularLonLatGrid*>(inputGrids.at(4));

    // Integrate geopotential height from surface to top. k = 0 denotes the
    // uppermost level, k = nlev-1 the lowest model level.

    // Start by computing the thickness of the layer between the surface and
    // the lowest model level.
    for (unsigned int j = 0; j < derivedGrid->getNumLats(); j++)
        for (unsigned int i = 0; i < derivedGrid->getNumLons(); i++)
        {
            unsigned int k_lowest = airTemperatureGrid->getNumLevels() - 1;
            float p_bot_hPa = surfaceAirPressureGrid->getValue(j, i) / 100.;
            float p_top_hPa = airTemperatureGrid->getPressure(k_lowest, j, i);

            // If pressure level data are used, the lower levels can be BELOW
            // the surface. Here we cannot compute geopotential height, the
            // corresponding values need to be set to M_MISSING_VALUE.
            while (p_top_hPa > p_bot_hPa)
            {
                derivedGrid->setValue(k_lowest, j, i, M_MISSING_VALUE);
                // Decrement k; pay attention to staying in the valid range.
                if (k_lowest == 0) continue; else k_lowest--;//Why continue not break?
                // Update current p_top_hPa.
                p_top_hPa = airTemperatureGrid->getPressure(k_lowest, j, i);
            }

//!TODO (mr, 11Mar2018) We're currently assuming specific humidity to be
//! constant in the lowermost layer. This needs to be replaced by an
//! implementation that uses surface dew point to compute virtual temperature.
//! The impact should be small at least for hybrid levels, though, as the
//! lowest layer usually is fairly thin.
            float virtualTemperature_bot_K = virtualTemperature_K(
                        surfaceTemperatureGrid->getValue(j, i),
                        specificHumidityGrid->getValue(k_lowest, j, i));

            float virtualTemperature_top_K = virtualTemperature_K(
                        airTemperatureGrid->getValue(k_lowest, j, i),
                        specificHumidityGrid->getValue(k_lowest, j, i));

            float layerMeanVirtualTemperature_K =
                    (virtualTemperature_bot_K + virtualTemperature_top_K) / 2.;

            float surfaceGeopotentialHeight_m =
                    surfaceGeopotentialGrid->getValue(j, i) /
                    MetConstants::GRAVITY_ACCELERATION;

            float geopotentialHeight_m = surfaceGeopotentialHeight_m +
                    geopotentialThicknessOfLayer_m(
                        layerMeanVirtualTemperature_K,
                        p_bot_hPa, p_top_hPa);

            derivedGrid->setValue(k_lowest, j, i, geopotentialHeight_m);
        }

    // Add thicknesses of all layers above.
    for (int k = int(derivedGrid->getNumLevels()) - 2; k >= 0; k--)
        for (unsigned int j = 0; j < derivedGrid->getNumLats(); j++)
            for (unsigned int i = 0; i < derivedGrid->getNumLons(); i++)
            {
                // Check if the bottom level of the current grid point has
                // already been flagged as missing value (.. pressure levels
                // .., see above).
                if (derivedGrid->getValue(k+1, j, i) == M_MISSING_VALUE)
                {
                    continue;
                }

                float p_bot_hPa = airTemperatureGrid->getPressure(k+1, j, i);
                float p_top_hPa = airTemperatureGrid->getPressure(k  , j, i);

                float virtualTemperature_bot_K = virtualTemperature_K(//Why define variables twice
                            airTemperatureGrid->getValue(k+1, j, i),
                            specificHumidityGrid->getValue(k+1, j, i));

                float virtualTemperature_top_K = virtualTemperature_K(
                            airTemperatureGrid->getValue(k, j, i),
                            specificHumidityGrid->getValue(k, j, i));

                float layerMeanVirtualTemperature_K =
                        (virtualTemperature_bot_K + virtualTemperature_top_K) / 2.;

                float geopotentialHeight_m =
                        derivedGrid->getValue(k+1, j, i) + // z of bot level
                        geopotentialThicknessOfLayer_m(
                            layerMeanVirtualTemperature_K,
                            p_bot_hPa, p_top_hPa);

                derivedGrid->setValue(k, j, i, geopotentialHeight_m);

//                // Debug output.
//                if (j == 0 && i == 0)
//                {
//                    LOG4CPLUS_DEBUG(mlog, "GEOP HEIGHT DBG: k=" << k
//                                    << " lev=" << derivedGrid->getLevels()[k]
//                                    << " p_bot=" << p_bot_hPa
//                                    << " p_top=" << p_top_hPa
//                                    << " T_bot=" << airTemperatureGrid->getValue(k+1, j, i)
//                                    << " q_bot=" << specificHumidityGrid->getValue(k+1, j, i)
//                                    << " T_top=" << airTemperatureGrid->getValue(k, j, i)
//                                    << " q_top=" << specificHumidityGrid->getValue(k, j, i)
//                                    << " Tv_bot=" << virtualTemperature_bot_K
//                                    << " Tv_top=" << virtualTemperature_top_K
//                                    << " Tv_mean=" << layerMeanVirtualTemperature_K
//                                    << " z(-1)=" << derivedGrid->getValue(k+1, j, i)
//                                    << " z=" << geopotentialHeight_m
//                                    << " \n" << flush);
//                }
            }
}


// Geopotential height from geopotential
// =====================================

MGeopotentialHeightFromGeopotentialProcessor
::MGeopotentialHeightFromGeopotentialProcessor()
    : MDerivedDataFieldProcessor(
          "geopotential_height_from_geopotential",
          QStringList() << "geopotential")
{
}

void MGeopotentialHeightFromGeopotentialProcessor::compute(
        QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "geopotential"

    for (unsigned int n = 0; n < derivedGrid->getNumValues(); n++)
    {
        float geopotential = inputGrids.at(0)->getValue(n);

        if (geopotential == M_MISSING_VALUE)
        {
            derivedGrid->setValue(n, M_MISSING_VALUE);
        }
        else
        {
            derivedGrid->setValue(n, geopotential /
                                  MetConstants::GRAVITY_ACCELERATION);
        }
    }
}


// Total precipitation per time interval
// =====================================

MTHourlyTotalPrecipitationProcessor::MTHourlyTotalPrecipitationProcessor(
        int hours)
    : MDerivedDataFieldProcessor(
          QString("lwe_thickness_of_precipitation_amount_%1h").arg(hours),
          QStringList() << "lwe_thickness_of_precipitation_amount"
                        << QString(
                               "lwe_thickness_of_precipitation_amount///-%1")
                           .arg(hours*3600))
{
}


void MTHourlyTotalPrecipitationProcessor::compute(
        QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "lwe_thickness_of_precipitation_amount"
    // input 1 = "lwe_thickness_of_precipitation_amount", valid - T hours

    if (inputGrids.at(0) == nullptr || inputGrids.at(1) == nullptr)
    {
        // In case the previous timestep is not available, a nullptr will be
        // passed as input. In this case, simply return a field of missing
        // values.
        derivedGrid->setToValue(M_MISSING_VALUE);
    }
    else for (unsigned int n = 0; n < derivedGrid->getNumValues(); n++)
    {
        float precip_VT = inputGrids.at(0)->getValue(n);
        float precip_VT_minus_Th = inputGrids.at(1)->getValue(n);

        float precip_difference = precip_VT - precip_VT_minus_Th;
        derivedGrid->setValue(n, precip_difference);
    }
}


// Vertically integrated moisture flux
// ===================================

// NOTE: surface_air_pressure is requested as a dummy grid to
// initialize the derived grid as a 2D field.
MMagnitudeOfVerticallyIntegratedMoistureFluxProcessor
::MMagnitudeOfVerticallyIntegratedMoistureFluxProcessor(QString levelTypeString)
    : MDerivedDataFieldProcessor(
          QString("magnitude_of_vertically_integrated_horizontal_"
                  "transport_of_moisture__from_%1").arg(levelTypeString),
          QStringList() << "surface_air_pressure"
                        << QString("eastward_wind/%1").arg(levelTypeString)
                        << QString("northward_wind/%1").arg(levelTypeString)
                        << QString("specific_humidity/%1").arg(levelTypeString))
{
}


void MMagnitudeOfVerticallyIntegratedMoistureFluxProcessor::compute(
        QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "surface_air_pressure" -- never used, just for initialization
    // input 1 = "eastward_wind"
    // input 2 = "northward_wind"
    // input 3 = "specific_humidity"
    MStructuredGrid *eastwardWindGrid = inputGrids.at(1);
    MStructuredGrid *northwardWindGrid = inputGrids.at(2);
    MStructuredGrid *specificHumidityGrid = inputGrids.at(3);

    for (unsigned int j = 0; j < derivedGrid->getNumLats(); j++)
        for (unsigned int i = 0; i < derivedGrid->getNumLons(); i++)
        {
            // For each horizontal grid point, compute the total
            // horizontal transport of moisture.
            // See: https://en.wikipedia.org/wiki/Moisture_advection#Moisture_flux
            // * horizontal moisture flux f = (fu, fv) = (u, v)
            //                              * mixing ratio / specific humidity
            // * vertical integral: int(psfc, 0, of: f/g dp)
            //
            // NOTE: This implementation used specific humidity; mixing ration
            //  can also be used.
            // Also cf. to Eq. (1) and (2) in Zebaze et al. (AtSciLet, 2017),
            // "Interaction between moisture transport...".

            float totalEastwardMoistureflux = 0.;
            float totalNorthwardMoistureflux = 0.;

            for (unsigned int k = 0; k < eastwardWindGrid->getNumLevels(); k++)
            {
                float layerDeltaPressure_Pa =
                        (eastwardWindGrid->getBottomInterfacePressure(k, j, i) -
                         eastwardWindGrid->getTopInterfacePressure(k, j, i)) * 100.;

                //float humidity = mixingRatio_kgkg(
                //            specificHumidityGrid->getValue(k, j, i));
                float humidity = specificHumidityGrid->getValue(k, j, i);

                totalEastwardMoistureflux +=
                        // eastward moisture flux = r*u
                        (humidity * eastwardWindGrid->getValue(k, j, i)) *
                        // * dp
                        layerDeltaPressure_Pa;

                totalNorthwardMoistureflux +=
                        // eastward moisture flux = r*v
                        (humidity * northwardWindGrid->getValue(k, j, i)) *
                        // * dp
                        layerDeltaPressure_Pa;
            }

            totalEastwardMoistureflux /= MetConstants::GRAVITY_ACCELERATION;
            totalNorthwardMoistureflux /= MetConstants::GRAVITY_ACCELERATION;

            // Computed total moisture flux magnitude is in [kg m-1 s-1].
            float totalMoistureFlux = pow(
                        totalEastwardMoistureflux * totalEastwardMoistureflux +
                        totalNorthwardMoistureflux * totalNorthwardMoistureflux,
                        0.5);

            static_cast<MRegularLonLatGrid*>(derivedGrid)->setValue(
                        j, i, totalMoistureFlux);
        }
}

//Isentropic Potential Vorticity (Later may use dew point for calculating sfc layer geopotential thickness)
// ===================================
MIsentropicPotentialVorticityProcessor
::MIsentropicPotentialVorticityProcessor()
    : MDerivedDataFieldProcessor(
          "ertel_potential_vorticity",
           QStringList() <<"air_temperature"
                        << "specific_humidity"
                        << "eastward_wind"
                       << "northward_wind"
                        << "surface_geopotential/SURFACE_2D"
                        << "surface_air_pressure/SURFACE_2D"
                        << "surface_temperature/SURFACE_2D")

                     //<< "air_pressure")
                   //<<"lat" use member function of structuredgrid class getNorthInterfaceLat
                  //<<"lon" getEastInterfaceLon
                 //<<"lev(model_level)") getNumLevels
{}
void MIsentropicPotentialVorticityProcessor::compute
(QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
   // input 0 = "air_temperature"
   // input 1 = "specific_humidity"
   // input 2 = "eastward_wind"
   // input 3 = "northward_wind"
   // input 4 = "surface_geopotential/SURFACE_2D"
   // input 5 = "surface_air_pressure/SURFACE_2D"
   // input 6 = "surface_temperature/SURFACE_2D"
   // Plan: Calculate geopotential height field
   //       Interpolate theta
   //       Find tangent vectors' components
   //       Interpolate winds
   //       Find grid size length
   //       Calculate relative vorticity
   //       Calculate potential vorticity
   MStructuredGrid *airTemperatureGrid = inputGrids.at(0);
   MStructuredGrid *specificHumidityGrid = inputGrids.at(1);
   MStructuredGrid *eastwardWindGrid = inputGrids.at(2);
   MStructuredGrid *northwardWindGrid = inputGrids.at(3);
   MRegularLonLatGrid *surfaceGeopotentialGrid =
       dynamic_cast<MRegularLonLatGrid*>(inputGrids.at(4));
   MRegularLonLatGrid *surfaceAirPressureGrid =
       dynamic_cast<MRegularLonLatGrid*>(inputGrids.at(5));
   MRegularLonLatGrid *surfaceTemperatureGrid =
       dynamic_cast<MRegularLonLatGrid*>(inputGrids.at(6));
   MStructuredGrid *geopotenitalHeightGrid=MIsentropicPotentialVorticityProcessor::createAndInitializeResultGrid(airTemperatureGrid);
   MStructuredGrid *potentialTemperatureGrid=MIsentropicPotentialVorticityProcessor::createAndInitializeResultGrid(airTemperatureGrid);
   MStructuredGrid *potentialVorticityGrid=MIsentropicPotentialVorticityProcessor::createAndInitializeResultGrid(airTemperatureGrid);
   MStructuredGrid *intermediateGrid=MIsentropicPotentialVorticityProcessor::createAndInitializeResultGrid(airTemperatureGrid);
   // To calculate geopotential height: 1.Calculate virtual tempertautre
    #pragma omp parallel
   {
    #pragma omp parallel for collapse(2)
   for (unsigned int j = 0; j < derivedGrid->getNumLats(); j++)
       for (unsigned int i = 0; i < derivedGrid->getNumLons(); i++)
       {
           unsigned int k_lowest = airTemperatureGrid->getNumLevels() - 1;
           float surfacePressure_hPa = surfaceAirPressureGrid->getValue(j, i) / 100.;
           float lowestPressure_hPa = airTemperatureGrid->getPressure(k_lowest, j, i);
           //while (lowestPressure_hPa > surfacePressure_hPa)
           //{
               //geopotenitalHeightGrid->setValue(k_lowest, j, i, M_MISSING_VALUE);
               // Decrement k; pay attention to staying in the valid range.
               //if (k_lowest == 0) continue;
               //else k_lowest--;//Why continue not break?
               // Update current p_top_hPa.
               //lowestPressure_hPa = airTemperatureGrid->getPressure(k_lowest, j, i);
           //}


           float surfaceVirtualTemperature_K=virtualTemperature_K(
                                                 surfaceTemperatureGrid->getValue(j,i),
                                                 specificHumidityGrid->getValue(k_lowest, j, i));
           float lowestVirtualTemperature_K=virtualTemperature_K(
                                                airTemperatureGrid->getValue(k_lowest,j,i),
                                                specificHumidityGrid->getValue(k_lowest, j, i));
           float layerMeanVirtualTemperature_K =
               (surfaceVirtualTemperature_K + lowestVirtualTemperature_K) / 2.;
           float surfaceGeopotentialHeight_m=surfaceGeopotentialGrid->getValue(j, i) /
                                             MetConstants::GRAVITY_ACCELERATION;
           float geopotentialHeight_m = surfaceGeopotentialHeight_m +
                                        geopotentialThicknessOfLayer_m(
                                            layerMeanVirtualTemperature_K,
                                            surfacePressure_hPa, lowestPressure_hPa);
           geopotenitalHeightGrid->setValue(k_lowest, j, i, geopotentialHeight_m);
       }
    }
    #pragma omp parallel
  {

   for(int k= int(geopotenitalHeightGrid->getNumLevels()) - 2; k >= 0; k--)
       #pragma omp for collapse(2)
       for (unsigned int j = 0; j < geopotenitalHeightGrid->getNumLats(); j++)
           for (unsigned int i = 0; i < geopotenitalHeightGrid->getNumLons(); i++)
           {
               // Check if the bottom level of the current grid point has
               // already been flagged as missing value (.. pressure levels
               // .., see above).
               if (geopotenitalHeightGrid->getValue(k+1, j, i) == M_MISSING_VALUE)
               {
                   continue;
               }

               float p_bot_hPa = airTemperatureGrid->getPressure(k+1, j, i);
               float p_top_hPa = airTemperatureGrid->getPressure(k  , j, i);

               float virtualTemperature_bot_K = virtualTemperature_K(//Why define variables twice
                                                    airTemperatureGrid->getValue(k+1, j, i),
                                                    specificHumidityGrid->getValue(k+1, j, i));

               float virtualTemperature_top_K = virtualTemperature_K(
                                                    airTemperatureGrid->getValue(k, j, i),
                                                    specificHumidityGrid->getValue(k, j, i));

               float layerMeanVirtualTemperature_K =
                   (virtualTemperature_bot_K + virtualTemperature_top_K) / 2.;

               float geopotentialHeight_m =
                   geopotenitalHeightGrid->getValue(k+1, j, i) + // z of bot level
                   geopotentialThicknessOfLayer_m(
                       layerMeanVirtualTemperature_K,
                       p_bot_hPa, p_top_hPa);

               geopotenitalHeightGrid->setValue(k, j, i, geopotentialHeight_m);
           }
   }
    #pragma omp parallel
   {
    #pragma omp for collapse(3)
   for(unsigned int k=0; k<geopotenitalHeightGrid->getNumLevels();k++)
       for (unsigned int j = 0; j < geopotenitalHeightGrid->getNumLats(); j++)
           for (unsigned int i = 0; i < geopotenitalHeightGrid->getNumLons(); i++){

               potentialTemperatureGrid->setValue(k, j, i, potentialTemperature_K(airTemperatureGrid->getValue(k, j, i),100*airTemperatureGrid->getPressure(k, j, i)));
           }
    }

    #pragma omp parallel
   {
    #pragma omp for collapse(3)
   for(unsigned int k= 0; k< derivedGrid->getNumLevels();  k++)
       for (unsigned int j = 0; j < derivedGrid->getNumLats(); j++)
           for (unsigned int i = 0; i < derivedGrid->getNumLons(); i++)
           {   float Z000_m=geopotenitalHeightGrid->getValue(k, j, i);

               float theta000_K=potentialTemperatureGrid->getValue(k, j, i);
               float Z100_m=0;
               float theta100_K=0;
               if(k==0){
                Z100_m=geopotenitalHeightGrid->getValue(k+1, j, i);
                theta100_K=potentialTemperatureGrid->getValue(k+1, j, i);
               }
               else{
                theta100_K=potentialTemperatureGrid->getValue(k-1, j, i);
                Z100_m=geopotenitalHeightGrid->getValue(k-1, j, i);
               }

               if(theta100_K==theta000_K){//avoid divided by zero
                    theta100_K=theta000_K*(1+1E-3);
               }
               float theta_interpolated_constZ_X01_K=0;
               float theta_interpolated_constZ_X10_K=0;

               //Interpolate theta at ZX01 which equals Z000 from upper and lower layer
               //theta_interpolated_constZ_X01_K=interpolation(Z000_m,ZLb_m,ZUb_m,thetaLb_K,thetaUb_K);
               theta_interpolated_constZ_X01_K=MIsentropicPotentialVorticityProcessor::interpolate1D(potentialTemperatureGrid,geopotenitalHeightGrid,Z000_m,k,j,i,derivedGrid->getNumLevels(),derivedGrid->getNumLats(),derivedGrid->getNumLons(),0,1);//dj=0,di=1
                //repeat for y
               theta_interpolated_constZ_X10_K=MIsentropicPotentialVorticityProcessor::interpolate1D(potentialTemperatureGrid,geopotenitalHeightGrid,Z000_m,k,j,i,derivedGrid->getNumLevels(),derivedGrid->getNumLats(),derivedGrid->getNumLons(),1,0);//dj=1,di=0
               float dZx_tangentVector_thetasfc_m=MetRoutinesExperimental::dZ_tangentVector_on_constTheta_surface(Z000_m,Z100_m,theta000_K,theta100_K,theta_interpolated_constZ_X01_K);
                     if (i==derivedGrid->getNumLons()-1){
                         dZx_tangentVector_thetasfc_m=-dZx_tangentVector_thetasfc_m;
                     }
               float Z001constThetaSfc_m=Z000_m+dZx_tangentVector_thetasfc_m;


               float dZy_tangentVector_thetasfc_m=MetRoutinesExperimental::dZ_tangentVector_on_constTheta_surface(Z000_m,Z100_m,theta000_K,theta100_K,theta_interpolated_constZ_X10_K);
                     if (j==derivedGrid->getNumLats()-1){
                     dZy_tangentVector_thetasfc_m=-dZy_tangentVector_thetasfc_m;
                     }
               float Z010constThetaSfc_m=Z000_m+dZy_tangentVector_thetasfc_m;
               //Start interpolate winds
               float v_interpolated_constTheta_Y01_ms=MIsentropicPotentialVorticityProcessor::interpolate1D(northwardWindGrid,geopotenitalHeightGrid,Z001constThetaSfc_m,k,j,i,derivedGrid->getNumLevels(),derivedGrid->getNumLats(),derivedGrid->getNumLons(),0,1);//dj=0,di=1
               //repeat for y
               float u_interpolated_constTheta_Y10_ms=MIsentropicPotentialVorticityProcessor::interpolate1D(eastwardWindGrid,geopotenitalHeightGrid,Z010constThetaSfc_m,k,j,i,derivedGrid->getNumLevels(),derivedGrid->getNumLats(),derivedGrid->getNumLons(),1,0);//dj=1,di=0
               float v000_ms=northwardWindGrid->getValue(k,j,i);
               float u000_ms=eastwardWindGrid->getValue(k,j,i);
               float dv_constTheta=v_interpolated_constTheta_Y01_ms-v000_ms;
               if (i==derivedGrid->getNumLons()-1){
                  dv_constTheta=-dv_constTheta;
               }
               float du_constTheta=u_interpolated_constTheta_Y10_ms-u000_ms;
               if(j==derivedGrid->getNumLats()-1){
                  du_constTheta=-du_constTheta;
               }
               float radius_m = MetConstants::EARTH_RADIUS_km * 1000.;
               float dx_model = gcDistance_deg(airTemperatureGrid->getEastInterfaceLon(i),
                                               airTemperatureGrid->getNorthInterfaceLat(j),
                                               airTemperatureGrid->getEastInterfaceLon(i + 1),
                                               airTemperatureGrid->getNorthInterfaceLat(j),
                                               radius_m);
               float dy_model = gcDistance_deg(airTemperatureGrid->getEastInterfaceLon(i),
                                               airTemperatureGrid->getNorthInterfaceLat(j),
                                               airTemperatureGrid->getEastInterfaceLon(i),
                                               airTemperatureGrid->getNorthInterfaceLat(j+1),
                                               radius_m);

               float dx_constTheta = MetRoutinesExperimental::vecLengthZ(dx_model, 0., dZx_tangentVector_thetasfc_m);
               float dy_constTheta = MetRoutinesExperimental::vecLengthZ(0.,dy_model,dZy_tangentVector_thetasfc_m);
               float pressure100_Pa=0;
               if (k==0){
                pressure100_Pa=airTemperatureGrid->getPressure(k+1, j, i)*100;
               }
               else{
               pressure100_Pa=airTemperatureGrid->getPressure(k-1, j, i)*100;
               }
               float pressure000_Pa=airTemperatureGrid->getPressure(k, j, i)*100;
               float dvdx_constTheta=dv_constTheta/dx_constTheta;
               float dudy_constTheta=du_constTheta/dy_constTheta;
               float relativevorticity=dvdx_constTheta-dudy_constTheta;
               float statisticStability=(theta100_K-theta000_K)/(pressure100_Pa-pressure000_Pa);
               float lat000_deg = airTemperatureGrid->getNorthInterfaceLat(j);
               float f = MetRoutinesExperimental::planetaryVorticity(lat000_deg);
               float isentropicPotentialVorticity=-1*MetConstants::GRAVITY_ACCELERATION*(relativevorticity+f)*statisticStability;
               potentialVorticityGrid->setValue(k, j, i, isentropicPotentialVorticity);
           }
   }//end parallel

    //No parallel
    for (unsigned int iter=0;iter<2;iter++){//setting no of smoothing
    MIsentropicPotentialVorticityProcessor::smooth(potentialVorticityGrid,intermediateGrid);
    MIsentropicPotentialVorticityProcessor::smooth(intermediateGrid,potentialVorticityGrid);
    }
#pragma omp parallel
{
#pragma omp for collapse(3)
for(unsigned int k= 0; k< derivedGrid->getNumLevels();  k++)
   for (unsigned int j = 0; j < derivedGrid->getNumLats(); j++)
       for (unsigned int i = 0; i < derivedGrid->getNumLons(); i++){
            derivedGrid->setValue(k, j, i, potentialVorticityGrid->getValue(k,j,i));
       }
    }


}

    MStructuredGrid* MIsentropicPotentialVorticityProcessor::createAndInitializeResultGrid (MStructuredGrid *templateGrid){
    MStructuredGrid *result = nullptr;

    switch (templateGrid->leveltype)
    {
    case PRESSURE_LEVELS_3D:
        result = new MRegularLonLatStructuredPressureGrid(
                    templateGrid->nlevs, templateGrid->nlats,
                    templateGrid->nlons);
        break;
    case HYBRID_SIGMA_PRESSURE_3D:
        result = new MLonLatHybridSigmaPressureGrid(
                    templateGrid->nlevs, templateGrid->nlats,
                    templateGrid->nlons);
        break;
    case AUXILIARY_PRESSURE_3D:
    {
        MLonLatAuxiliaryPressureGrid *templateGridAux =
                dynamic_cast<MLonLatAuxiliaryPressureGrid*>(templateGrid);
        result = new MLonLatAuxiliaryPressureGrid(
                    templateGrid->nlevs, templateGrid->nlats,
                    templateGrid->nlons, templateGridAux->reverseLevels);
        break;
    }
    case POTENTIAL_VORTICITY_2D:
        break;
    case SURFACE_2D:
        result = new MRegularLonLatGrid(templateGrid->nlats,
                                        templateGrid->nlons);
        break;
    case LOG_PRESSURE_LEVELS_3D:
        result = new MRegularLonLatLnPGrid(
                    templateGrid->nlevs, templateGrid->nlats,
                    templateGrid->nlons);
        break;
    default:
        break;
    }

    if (result == nullptr)
    {
        QString msg = QString("ERROR: Cannot intialize result grid. Level "
                              "type %1 not implemented.")
                .arg(MStructuredGrid::verticalLevelTypeToString(
                         templateGrid->leveltype));
        LOG4CPLUS_ERROR(mlog, msg.toStdString());
        throw MInitialisationError(msg.toStdString(), __FILE__, __LINE__);
    }

    // Copy coordinate axes.
    for (unsigned int i = 0; i < templateGrid->nlons; i++)
        result->lons[i] = templateGrid->lons[i];
    for (unsigned int j = 0; j < templateGrid->nlats; j++)
        result->lats[j] = templateGrid->lats[j];
    for (unsigned int i = 0; i < templateGrid->nlevs; i++)
        result->levels[i] = templateGrid->levels[i];

    result->setAvailableMembers(templateGrid->getAvailableMembers());

    if (templateGrid->leveltype == HYBRID_SIGMA_PRESSURE_3D)
    {
        // Special treatment for hybrid model levels: copy ak/bk coeffs.
        MLonLatHybridSigmaPressureGrid *hybtemplate =
                dynamic_cast<MLonLatHybridSigmaPressureGrid*>(templateGrid);
        MLonLatHybridSigmaPressureGrid *hybresult =
                dynamic_cast<MLonLatHybridSigmaPressureGrid*>(result);
        for (unsigned int i = 0; i < hybtemplate->nlevs; i++)
        {
            hybresult->ak_hPa[i] = hybtemplate->ak_hPa[i];
            hybresult->bk[i] = hybtemplate->bk[i];
        }

        // Take care of the surface grid: use the surface grid of the template
        // grid.
        hybresult->surfacePressure = hybtemplate->getSurfacePressureGrid();

        // Increase the reference counter for this field (as done above by
        // containsData() or storeData()). NOTE: The field is released in
        // the destructor of "result" -- the reference is kept for the
        // entire lifetime of "result" to make sure the psfc field is not
        // deleted while "result" is still in memory.
        if ( !hybresult->surfacePressure->increaseReferenceCounter() )
        {
            // This should not happen.
            QString msg = QString("This is embarrassing: The data item "
                                  "for request %1 should have been in "
                                  "cache.").arg(
                        hybresult->surfacePressure->getGeneratingRequest());
            throw MMemoryError(msg.toStdString(), __FILE__, __LINE__);
        }
    } // grid is hybrid sigma pressure

    else if (templateGrid->leveltype == AUXILIARY_PRESSURE_3D)
    {
        // Special treatment for auxiliary pressure levels: copy pointer to
        // auxiliary 3D pressure field.
        MLonLatAuxiliaryPressureGrid *auxptemplate =
                dynamic_cast<MLonLatAuxiliaryPressureGrid*>(templateGrid);
        MLonLatAuxiliaryPressureGrid *auxresult =
                dynamic_cast<MLonLatAuxiliaryPressureGrid*>(result);
        auxresult->auxPressureField_hPa =
                auxptemplate->getAuxiliaryPressureFieldGrid();

        // Increase the reference counter for this field (as done above by
        // containsData() or storeData()). NOTE: The field is released in
        // the destructor of "result" -- the reference is kept for the
        // entire lifetime of "result" to make sure the pressure field is
        // not deleted while "result" is still in memory.
        if ( !auxresult->auxPressureField_hPa->increaseReferenceCounter() )
        {
            // This should not happen.
            QString msg = QString("This is embarrassing: The data item "
                                  "for request %1 should have been in "
                                  "cache.").arg(
                        auxresult->auxPressureField_hPa->getGeneratingRequest());
            throw MMemoryError(msg.toStdString(), __FILE__, __LINE__);
        }
    } // grid is auxiliary pressure 3D

    return result;
}
    float MIsentropicPotentialVorticityProcessor::interpolate1D(MStructuredGrid * targetGrid,  MStructuredGrid* knownGrid,float targetx,
                        unsigned int k,unsigned int j,unsigned int i,
                        unsigned int numLevels,unsigned int numLats,unsigned int numLons,int dj,int di){
                            //Goals: 1.Encapsulation 2. Handle boundary case 3.use a template for two versions?/overload, or separate method for common work Make a grid of potential temperature

        //int di=1;
        if(i==numLons-1&&di==1){
            di=-1;
        }
        //int dj=0;
        if(j==numLats-1&&dj==1){
            dj=-1;
        }
        float neighbourx=knownGrid->getValue(k,j+dj,i+di);
        //int dk;
        unsigned int k_lowest=numLevels-1;
        unsigned int k_highest=0;
        float topx=knownGrid->getValue(k_highest,j+dj,i+di);
        float bottomx=knownGrid->getValue(k_lowest,j+dj,i+di);
        float result_consty=0;
        if(neighbourx==targetx){
            result_consty=targetGrid->getValue(k,j+dj,i+di);
        }
        else{
            if (neighbourx>targetx){
                if(bottomx>targetx|| k==k_lowest){//special case if the targetx is below the lowest layer and the current layer is the bottom layer
                    float lowerx=knownGrid->getValue(k_lowest, j+dj, i+di);
                    float lowerPlus1x=knownGrid->getValue(k_lowest-1,j+dj,i+di);
                    float lowery=targetGrid->getValue(k_lowest,j+dj,i+di);
                    float lowerPlus1y=targetGrid->getValue(k_lowest-1,j+dj,i+di);
                    result_consty=MetRoutinesExperimental::interpolation(targetx,lowerx,lowerPlus1x,lowery,lowerPlus1y);
                }
                else{//general case bottomx<targetx
                    float lowerx=knownGrid->getValue(k+1, j+dj, i+di);
                    int n=k+1;
                    for(; !(lowerx<targetx); n++){//going down
                        lowerx=knownGrid->getValue(n+1, j+dj, i+di);

                    }
                    float lowerPlus1x=knownGrid->getValue(n-1, j+dj, i+di);
                    float lowery=targetGrid->getValue(n,j+dj,i+di);
                    float lowerPlus1y=targetGrid->getValue(n-1,j+dj,i+di);
                    result_consty=MetRoutinesExperimental::interpolation(targetx,lowerx,lowerPlus1x,lowery,lowerPlus1y);
                }
            }
            else{//neighbourx<targetx
                    if(topx<targetx||k==k_highest){
                        float higherx=knownGrid->getValue(k_highest,j+dj,i+di);
                        float higherMinus1x=knownGrid->getValue(k_highest+1,j+dj,i+di);
                        float highery=targetGrid->getValue(k_highest,j+dj,i+di);
                        float higherMinus1y=targetGrid->getValue(k_highest+1,j+dj,i+di);
                        result_consty=MetRoutinesExperimental::interpolation(targetx,higherMinus1x,higherx,higherMinus1y,highery);
                    }else{
                         float higherx=knownGrid->getValue(k-1, j+dj, i+di);
                         int n =k-1;
                         for(; ! (higherx>targetx); n--)
                               {
                                   higherx=knownGrid->getValue(n-1, j+dj, i+di);
                               }
                         float higherMinus1x=knownGrid->getValue(n+1, j+dj, i+di);
                         float highery=targetGrid->getValue(n, j+dj, i+di);
                         float higherMinus1y=targetGrid->getValue(n+1, j+dj, i+di);
                         result_consty=MetRoutinesExperimental::interpolation(targetx,higherMinus1x,higherx,higherMinus1y,highery);
                    }

            }


        }

    return result_consty;

    }

    void MIsentropicPotentialVorticityProcessor::smooth(MStructuredGrid* inputGrid,MStructuredGrid * outputGrid){
#pragma omp parallel
        {
          #pragma omp for collapse(3)
        for(unsigned int k= 0; k< inputGrid->getNumLevels();  k++)
            for (unsigned int j = 0; j < inputGrid->getNumLats(); j++)
                for (unsigned int i = 0; i < inputGrid->getNumLons(); i++){
                    float neighbourmean=0;
                    float numNeighbour=0;
                    float local=inputGrid->getValue(k,j,i);
                    float smoothRes=0;
                    float weight=0;
                    if(i==0){
                        if(j==0){//i==0 and j==0
                            neighbourmean=inputGrid->getValue(k,j+1,i+1)+inputGrid->getValue(k,j,i+1)+inputGrid->getValue(k,j+1,i);
                            numNeighbour=3;
                        }else{
                            if(j==inputGrid->getNumLats()-1){//i==0 and j==inputGrid->getNumLats()-1
                                neighbourmean=inputGrid->getValue(k,j,i+1)+inputGrid->getValue(k,j-1,i)+inputGrid->getValue(k,j-1,i+1);
                                numNeighbour=3;
                            }else{//i==0 and j!=boundary
                                neighbourmean=inputGrid->getValue(k,j+1,i+1)+inputGrid->getValue(k,j,i+1)+inputGrid->getValue(k,j-1,i)+inputGrid->getValue(k,j-1,i+1)+inputGrid->getValue(k,j+1,i);
                                numNeighbour=5;

                            }
                        }

                    }else{
                        if(i==inputGrid->getNumLons()-1){
                            if(j==0){//i==inputGrid->getNumLons()-1 and j==0
                                neighbourmean=inputGrid->getValue(k,j+1,i)+inputGrid->getValue(k,j+1,i-1)+inputGrid->getValue(k,j,i-1);
                                numNeighbour=3;
                            }else{
                                if(j==inputGrid->getNumLats()-1){//i==inputGrid->getNumLons()-1 and j==inputGrid->getNumLats()-1
                                    neighbourmean=inputGrid->getValue(k,j-1,i)+inputGrid->getValue(k,j-1,i-1)+inputGrid->getValue(k,j,i-1);
                                    numNeighbour=3;
                                }else{//i==inputGrid->getNumLons()-1,j!=boundary
                                    neighbourmean=inputGrid->getValue(k,j-1,i)+inputGrid->getValue(k,j-1,i-1)+inputGrid->getValue(k,j+1,i-1)+inputGrid->getValue(k,j,i-1)+inputGrid->getValue(k,j+1,i);
                                    numNeighbour=5;
                                }
                            }
                        }else{
                        if(j==0){//i!=boundary and j==0
                                neighbourmean=inputGrid->getValue(k,j,i+1)+inputGrid->getValue(k,j,i-1)+inputGrid->getValue(k,j+1,i-1)+inputGrid->getValue(k,j+1,i+1)+inputGrid->getValue(k,j+1,i);
                                numNeighbour=5;
                        }else{
                            if(j==inputGrid->getNumLats()-1){//i!=boundary and j==inputGrid->getNumLats()-1
                                neighbourmean=inputGrid->getValue(k,j,i+1)+inputGrid->getValue(k,j,i-1)+inputGrid->getValue(k,j-1,i+1)+inputGrid->getValue(k,j-1,i)+inputGrid->getValue(k,j-1,i-1);
                                numNeighbour=5;
                            }else{//i!=boundary,j!=boundary
                                neighbourmean=inputGrid->getValue(k,j,i+1)+inputGrid->getValue(k,j,i-1)+inputGrid->getValue(k,j-1,i+1)+inputGrid->getValue(k,j-1,i)+inputGrid->getValue(k,j-1,i-1)+inputGrid->getValue(k,j+1,i+1)+inputGrid->getValue(k,j+1,i)+inputGrid->getValue(k,j+1,i-1);
                                numNeighbour=8;
                            }
                        }
                    }
                }
            neighbourmean=neighbourmean/numNeighbour;
            weight=1/(numNeighbour+1);
            smoothRes=weight*local+(1-weight)*neighbourmean;
            outputGrid->setValue(k, j, i, smoothRes);
      }
    }
    }
} // namespace Met3D

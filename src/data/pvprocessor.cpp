//Isentropic Potential Vorticity (Later may use dew point for calculating sfc layer geopotential thickness)
// ===================================
MIsentropicPotentialVorticityProcessor
::MIsentropicPotentialVorticityProcessor()
    : MDerivedDataFieldProcessor(
          "ertel_potential_vorticity",
           QStringList() <<"air_temperature"
                        << "specific_humidity"
                        <<"eastward_wind"
                       <<"northward_wind"
                      <<"surface_air_pressure"
                     <<"air_pressure"
                    <<"geopotential")
                   //<<"lat" use member function of structuredgrid class getNorthInterfaceLat
                  //<<"lon" getEastInterfaceLon
                 //<<"lev(model_level)") getNumLevels
{}
void MIsentropicPotentialVorticityProcessor::compute
(QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "air_temperature"
    // input 1 = "specific_humidity"
    // input 2 = "eastward_wind"
    // input 3 = "northward_wind"
    //
    // input  = "air_pressure"
    // input 4 = "geopotential" use the output of geopotential processor.
    // input 5 = "surface_air_pressure"
    MStructuredGrid *airTemperatureGrid = inputGrids.at(0);
    MStructuredGrid *specificHumidityGrid = inputGrids.at(1);
    MStructuredGrid *eastwardWindGrid = inputGrids.at(2);
    MStructuredGrid *northwardWindGrid = inputGrids.at(3);
    MStructuredGrid *geopotnetialHeightGrid = inputGrids.at(4);
    MRegularLonLatGrid *surfaceAirPressureGrid =
            dynamic_cast<MRegularLonLatGrid*>(inputGrids.at(5));
    for (unsigned int k = 1; k < derivedGrid->getNumLevels()-1; k++){//start from the 2nd level of atm, k=1 to k=NumLevels-1
        for (unsigned int j = 0; j < derivedGrid->getNumLats()-1; j++){// minus 1 due to first order calculation
            for (unsigned int i = 0; i < derivedGrid->getNumLons()-1; i++){
                // Calculate theta at constant geopotential height
                // Consider current point first, assume it is in the middle not at the boundaries
                // More careful for lowest/uppermost level
                // Consider x direction first
                float geopotentialheight000= geopotnetialHeightGrid->getValue(k,j,i);
                float geopotentialheight001= geopotnetialHeightGrid->getValue(k,j,i+1);
                // Find the level of the current geopotential height (Z) at ?01
                // Find the interval of model levels that encloses current geopotential height Z000
		// case 0: dZ/dx = 0
		if (geopotentialheight001==geopotentialheight000){
			float potentialtempatconstheightX01=potentialTemperature_K(airTemperatureGrid->getValue(n,j,i+1),airTemperatureGrid->getPressure(n,j,i+1));
		}
                // case 1: dZ/dx slope > 0 search downward
                if (geopotentialheight001>geopotentialheight000){
                    for (unsigned int n=k+1;n<derivedGrid->getNumLevels();n++){
                        if(geopotentialheight000>geopotnetialHeightGrid->getValue(n,j,i+1)){//Lower level n, upper level n-1
                            // theta?01 = theta_low + (theta_hi- theta_low)*(Z000 - Zlow /Zhi - Zlow)
                            // Interpolate theta of current geopotential height from two neighbouring
                            // known geopotential heights from model layer
                            float potentialtempatconstheightX01=
                                    potentialTemperature_K(airTemperatureGrid->getValue(n,j,i+1),airTemperatureGrid->getPressure(n,j,i+1))+
                                    (potentialtemp((n-1,j,i+1))-potentialtemp((n,j,i+1)))*
                                    (geopotentialheight000-geopotnetialHeightGrid->getValue(n,j,i+1))/
                                    (geopotnetialHeightGrid->getValue(n-1,j,i+1)-geopotnetialHeightGrid->getValue(n,j,i+1));
                            break;
                        }
                    }
                }
                // case 2: dZ/dx slope < 0, search upwards
                if (geopotentialheight001<geopotentialheight000){
                    for (unsigned int n=k-1;n>0;n--){
                        if(geopotentialheight000<geopotnetialHeightGrid->getValue(n,j,i+1)){//Lower level n+1, upper level n
                            float potentialtempatconstheightX01=potentialtemp((n+1,j,i+1))+
                                    (potentialtemp((n,j,i+1))-potentialtemp((n+1,j,i+1)))*
                                    (geopotentialheight000-geopotnetialHeightGrid->getValue(n+1,j,i+1))/
                                    (geopotnetialHeightGrid->getValue(n,j,i+1)-geopotnetialHeightGrid->getValue(n+1,j,i+1));
                            break;
                        }
                    }
                }

                // Repeat for the y direction
                float geopotentialheight010= geopotnetialHeightGrid->getValue(k,j+1,i);
                // case 1: dZ/dy slope > 0 search downward
                if (geopotentialheight010>geopotentialheight000){
                    for (unsigned int n=k+1;n<derivedGrid->getNumLevels();n++){
                        if(geopotentialheight000>geopotnetialHeightGrid->getValue(n,j+1,i)){//Lower level n, upper level n-1
                            float potentialtempatconstheightX10=potentialtemp((n,j+1,i))+
                                    (potentialtemp((n-1,j+1,i))-potentialtemp((n,j+1,i)))*
                                    (geopotentialheight000-geopotnetialHeightGrid->getValue(n,j+1,i))/
                                    (geopotnetialHeightGrid->getValue(n-1,j+1,i)-geopotnetialHeightGrid->getValue(n,j+1,i));

                            //Found the interval of model levels that encloses current geopotential height,
                            //Interpolate theta to current geopotential height at point ?10
                            break;
                        }
                    }
                }
                // case 2: dZ/dy slope < 0, search upwards
                if (geopotentialheight010<geopotentialheight000){
                    for (unsigned int n=k-1;n>0;n--){
                        if(geopotentialheight000<geopotnetialHeightGrid->getValue(n,j+1,i)){//Lower level n+1, upper level n
                            float potentialtempatconstheightX10=potentialtemp((n+1,j+1,i))+
                                    (potentialtemp((n,j+1,i))-potentialtemp((n+1,j+1,i)))*
                                    (geopotentialheight000-geopotnetialHeightGrid->getValue(n+1,j+1,i))/
                                    (geopotnetialHeightGrid->getValue(n,j+1,i)-geopotnetialHeightGrid->getValue(n+1,j+1,i));
                            break;
                        }
                    }
                }
                // Calculated neigbouring thetas in constant geopotential height in x and y directions
                // Find the tangent line of constant theta in x-Z plane
                //
                // method from https://www.meteo.physik.uni-muenchen.de/DokuWiki/lib/exe/fetch.php?media=intern:abschlussarbeiten:2017:ba2017_kriegmair_raphael.pdf
                // Raphael's Bachelors thesis appendix A
                // dZmodel/dtheta= difference of geopotential height of model levels/difference of theta of model levels
                // dZx= dZm/dtheta * (current potentialtemp-potentialtempatconstheightX10)
                float geopotentialheight100= geopotnetialHeightGrid->getValue(k-1,j,i);// Error: k=0
                float potentialtemp000=potentialtemp((k,j,i));
                float potentialtemp100=potentialtemp((k-1,j,i));
                float dZm =geopotentialheight100-geopotentialheight000;
                float dtheta =potentialtemp100-potentialtemp000;
                float dZx= dZm / dtheta *(potentialtemp000-potentialtempatconstheightX01);
                float dZy= dZm / dtheta *(potentialtemp000-potentialtempatconstheightX10);
                //Find the model levels that enclose Z000+dZx at ?01
                if (dZx>0)//search upwards
                {
                    for (unsigned int n=k-1;n>0;n--){//stop if Z+dZx<Z
                        if(geopotentialheight000+dZx<geopotnetialHeightGrid->getValue(n,j,i+1)){//Lower level n+1, upper level n
                            float vwindY01=northwardWindGridvwind->getValue((n+1,j,i+1))+
                                    (vwind((n,j,i+1))-vwind((n+1,j,i+1)))*
                                    (geopotentialheight000+dZx-geopotnetialHeightGrid->getValue(n+1,j,i+1))/
                                    (geopotnetialHeightGrid->getValue(n,j,i+1)-geopotnetialHeightGrid->getValue(n+1,j,i+1));
                            break;
                        }
                    }

                }
                if (dZx<0)//search downwards
                {
                    for (unsigned int n=k+1;n<derivedGrid->getNumLevels();n++){
                        if(geopotentialheight000+dZx>geopotnetialHeightGrid->getValue(n,j,i+1)){//Lower level n, upper level n-1
                            float vwindY01=vwind((n,j,i+1))+
                                    (vwind((n-1,j,i+1))-vwind((n,j,i+1)))*
                                    (geopotentialheight000+dZx-geopotnetialHeightGrid->getValue(n,j,i+1))/
                                    (geopotnetialHeightGrid->getValue(n-1,j,i+1)-geopotnetialHeightGrid->getValue(n,j,i+1));

                            break;
                        }
                    }
                }
                // Find the model levels that enclose Z000+dZy at ?10
                if (dZy>0)//search upwards
                {
                    for (unsigned int n=k-1;n>0;n--){//stop if Z+dZy<Z
                        if(geopotentialheight000+dZy<geopotnetialHeightGrid->getValue(n,j+1,i)){//Lower level n+1, upper level n
                            float uwindY10=uwind((n+1,j+1,i))+
                                    (uwind((n,j+1,i))-uwind((n+1,j+1,i)))*
                                    (geopotentialheight000+dZy-geopotnetialHeightGrid->getValue(n+1,j+1,i))/
                                    (geopotnetialHeightGrid->getValue(n,j+1,i)-geopotnetialHeightGrid->getValue(n+1,j+1,i));
                            break;
                        }
                    }

                }
                if (dZy<0)//search downwards
                {
                    for (unsigned int n=k+1;n<derivedGrid->getNumLevels();n++){
                        if(geopotentialheight000+dZy>geopotnetialHeightGrid->getValue(n,j+1,i)){//Lower level n, upper level n-1
                            float uwindY10=uwind((n,j+1,i))+
                                    (uwind((n-1,j+1,i))-uwind((n,j+1,i)))*
                                    (geopotentialheight000+dZy-geopotnetialHeightGrid->getValue(n,j+1,i))/
                                    (geopotnetialHeightGrid->getValue(n-1,j+1,i)-geopotnetialHeightGrid->getValue(n,j+1,i));

                            break;
                        }
                    }
                }

                float dvwind = vwindY01-vwind000;
                float duwind = uwindY10-uwind000;
                float radius_m = MetConstants::EARTH_RADIUS_km * 1000.;
                float dx_m=gcDistance_deg(inputGrids.at(0)->getEastInterfaceLon(i),
                                          inputGrids.at(0)->getNorthInterfaceLat(j),
                                          inputGrids.at(0)->getEastInterfaceLon(i + 1),
                                          inputGrids.at(0)->getNorthInterfaceLat(j),
                                          radius_m);;//distance between 000 to 001 in model grids
                float dy_m=gcDistance_deg(inputGrids.at(0)->getEastInterfaceLon(i),
                                          inputGrids.at(0)->getNorthInterfaceLat(j),
                                          inputGrids.at(0)->getEastInterfaceLon(i),
                                          inputGrids.at(0)->getNorthInterfaceLat(j + 1),
                                          radius_m);//distance between 000 to 010 in model grids
                float dx=vecLengthZ(dx_m, 0., dZx);//geometric length of dx
                float dy=vecLengthZ(0., dy_m, dZy);//geometric length of dy
                float relativevorticity=dvwind/dx - duwind / dy;
                float dp=airTemperatureGrid->getPressure(k-1,j,i)-airTemperatureGrid->getPressure(k,j,i);//upper-lower
                float coriolisparameter=2*MetConstants::EARTH_ROTATION_RATE*sin(inputGrids.at(0)->getNorthInterfaceLat(j)*M_PI/180.);
                float staticstability=dtheta/dp;//dtheta=model level potential temperature difference
                float IPV=-MetConstants::GRAVITY_ACCELERATION*(relativevorticity+coriolisparameter)*staticstability;
                derivedGrid->setValue(k, j, i, IPV);
                    }}}


}

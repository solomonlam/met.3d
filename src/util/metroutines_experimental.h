/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2018 Marc Rautenhaus
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef METROUTINES_EXPERIMENTAL_H
#define METROUTINES_EXPERIMENTAL_H

// standard library imports

// related third party imports

// local application imports
#include "util/mutil.h"
#include "metroutines.h"
/**
  NOTE: Use this file to implement experimental new meteorological computations
  that need testing before going into "metroutines.h/.cpp".
 */

namespace Met3D
{

/******************************************************************************
***                               CONSTANTS                                 ***
*******************************************************************************/

namespace MetConstants
{
const double earthRotationRate=7.2921e-5;
}


/******************************************************************************
***                                METHODS                                  ***
*******************************************************************************/

namespace MetRoutinesExperimental
{
double interpolation(double xTarget,double xLb, double xUb,double yLb,double yUb);
double dZ_tangentVector_on_constTheta_surface(double Z000,double Z100,double theta000,double theta100, double theta_interpolated_constZ);
double Z_on_constTheta_surface(double Z000,double Z100,double theta000,double theta100, double theta_interpolated_constZ);
double vecLengthZ(double x,double y,double z);
double planetaryVorticity(double lat_deg);
}

} // namespace Met3D

#endif // METROUTINES_EXPERIMENTAL_H

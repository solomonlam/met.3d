# Configure available memory managers.
# =============================================================================

[MemoryManager]
size=3

# Numerical weather prediction memory manager gets 10 GB system memory.
1\name=NWP
1\size_MB=10240

# Memory manager that caches analysis results (10 MB are sufficient).
2\name=Analysis
2\size_MB=10

# Memory manager for trajectory data.
#3\name=Trajectories DF-T psfc_1000hPa_L62
#3\size_MB=10240


# Configure data processing pipelines.
# =============================================================================

[NWPPipeline]
size=2

# Example configuration for a dataset.
# ------------------------------------
# Specifiy a name for the dataset.
1\name=forecast0925
# Specify the directory in which the data files are stored. If you specify
# a file filter (wildcards allowed), only those files that correspond to the
# filter will be used.
# Special cases:
#  If files are distributed over multiple subdirectories, use "/" as the
# path separator. Example: "/my_forecast.grb".
#  For ensemble datasets that do NOT store the ensemble dimension in their 
# NetCDF/GRIB files but instead have the ensemble member ID encoded in their 
# file or subdirectory names (e.g., "my_forecast.004.grb" or
# "member_004/temperature.nc"), the position of the ensemble ID can be specified
# by placing "\\e" in the file filter string. Example: "my_forecast.\\e.grb" or
# "member_\\e/temperature.nc". Note: At the moment Met.3D only allows positive
# integers as ensemble identifiers (leading zeros allowed).
1\path="/project/meteo/scratch/C.Lam/links to grb/forecast"
1\fileFilter="20160925_*_ecmwf_ensemble_forecast.*NAWDEX_LLML.00*.grb"
#/2016092*_*_ecmwf_ensemble_forecast.*NAWDEX_LLML.003.grb
# schedulerID can be "MultiThread" or "SingleThread".
1\schedulerID=MultiThread
# Specify the ID of a memory manager that is defined above.
1\memoryManagerID=NWP
# fileFormat can be "CF_NETCDF" or "ECMWF_GRIB".
1\fileFormat=ECMWF_GRIB
# Set enableRegridding to "true" if the regridding module should be activated.
1\enableRegridding=true
# Set "enableProbabilityRegionFilter" to "true" if you want to use the
# probability region detection module.
1\enableProbabilityRegionFilter=false
# Set "treatRotatedGridAsRegularGrid" to "true" if you want to treat rotated
# grids as regular grids. Setting it to false will cause rejection of data sets
# with rotated grid coordinates.
1\treatRotatedGridAsRegularGrid=false
# For ECMWF grib files only: specify type and name of the surface pressure field
# used to reconstruct pressure for hybrid sigma-pressure coordinates.
# Can be "sp" (surface pressure), "lnsp" (log of surface pressure) or
# "auto" (Met.3D tries to detect the surface pressure field
# (NOTE: this can be slow!)).
1\gribSurfacePressureFieldType="sp"
# Set "convertGeometricHeightToPressure_ICAOStandard" to "true" to load data
# using geometric height as vertical coordinate. Metres are converted to
# pressure using the ICAO standard atmosphere. (NOTE: the resulting pressure is
# hence approximate!)
1\convertGeometricHeightToPressure_ICAOStandard=false
# Name of an optional auxiliary 3D pressure field. Use this flag for datasets
# whose variables require an explicit definition of grid point pressure, e.g.,
# grids defined on (hybrid) geometric coordinates. Currently only implemented
# for NetCDF files. Leave empty to not use any auxiliary field. Cannot be used
# together with convertGeometricHeightToPressure_ICAOStandard.
1\auxiliary3DPressureField=
# Grid consistency check ensures that all variables of one dataset are defined
# on the same grid (lons, lats, levels, etc.). Disabling it might result in
# wrong behaviour of vertical cross-section actors (using variables defined on
# different grids in one actor), derived variables (variables used to calculate
# derived variables defined on different grids) and auxiliary 3D pressure fields
# (connection to variables defined on different grids than the auxiliary
# pressure field). Disabling can be necessary to use data that is stored on
# staggered grids, however, in this case, the staggering will be ignored.
# Hence, only disable if you know what you are doing.
1\disableGridConsistencyCheck=false
# To enable the built-in computation of derived variables (wind speed, potential
# temperature, etc.), Met.3D needs to know which variables in this dataset
# denote which quantity. E.g., what is the name of the eastward wind component
# in the NetCDF files of this dataset? In the following, provide a string
# of "/"-separatedpairs"CF_standard_name:Dataset_variable_name" such as
# "eastward_wind:u-component_of_wind_hybrid".
1\inputVarsForDerivedVars=eastward_wind:u (ens)/northward_wind:v (ens)/air_temperature:t (ens)/specific_humidity:q (ens)/surface_geopotential:z (ens)/surface_air_pressure:sp (ens)/surface_temperature:2t (ens)/#lwe_thickness_of_precipitation_amount:Total_precipitation_surface

# Example configuration for an ensemble ECMWF CF-NetCDF dataset.
2\name=analysis
2\path="/project/meteo/scratch/C.Lam/links to grb/analysis/"
2\fileFilter=ecmwf*
2\schedulerID=MultiThread
2\memoryManagerID=NWP
2\fileFormat=ECMWF_GRIB
2\enableRegridding=true
2\gribSurfacePressureFieldType="lnsp"
2\enableProbabilityRegionFilter=false
2\inputVarsForDerivedVars=eastward_wind:u (an)/northward_wind:v (an)/air_temperature:t (an)/specific_humidity:q (an)/surface_geopotential:z (an)/surface_air_pressure:sp (an)/surface_temperature:skt (an)/#lwe_thickness_of_precipitation_amount:Total_precipitation_surface

3\name=forecast0926
3\path="/project/meteo/scratch/C.Lam/links to grb/forecast"
3\fileFilter="20160926_*_ecmwf_ensemble_forecast.*NAWDEX_LLML.00*.grb"
3\schedulerID=MultiThread
3\memoryManagerID=NWP
3\fileFormat=ECMWF_GRIB
3\enableRegridding=true
3\enableProbabilityRegionFilter=false
3\treatRotatedGridAsRegularGrid=false
3\gribSurfacePressureFieldType="sp"
3\convertGeometricHeightToPressure_ICAOStandard=false
3\inputVarsForDerivedVars=eastward_wind:u (ens)/northward_wind:v (ens)/air_temperature:t (ens)/specific_humidity:q (ens)/surface_geopotential:z (ens)/surface_air_pressure:sp (ens)/surface_temperature:2t (ens)/#lwe_thickness_of_precipitation_amount:Total_precipitation_surface

# Example configuration for an ensemble ECMWF CF-NetCDF dataset.
3\name=analysis
3\path="/project/meteo/scratch/C.Lam/links to grb/analysis/"
3\fileFilter=ecmwf*
3\schedulerID=MultiThread
3\memoryManagerID=NWP
3\fileFormat=ECMWF_GRIB
3\enableRegridding=true
3\gribSurfacePressureFieldType="lnsp"
3\enableProbabilityRegionFilter=false
3\inputVarsForDerivedVars=eastward_wind:u (an)/northward_wind:v (an)/air_temperature:t (an)/specific_humidity:q (an)/surface_geopotential:z (an)/surface_air_pressure:sp (an)/surface_temperature:skt (an)/#lwe_thickness_of_precipitation_amount:Total_precipitation_surface

3\name=Offline calculation
3\path="/project/meteo/scratch/C.Lam/output/calculated data"
3\fileFilter=20160925_00_ecmwf_ensemble_forecast.NAWDEX_LLML.000.forecasttheta2*
3\schedulerID=MultiThread
3\memoryManagerID=NWP
3\fileFormat=CF_NETCDF
3\enableRegridding=false
3\enableProbabilityRegionFilter=false
3\enableRegridding=true


[LagrantoPipeline]
size=0

1\name=Lagranto ENS EUR_LL10 DF-T psfc_1000hPa_L62
1\ensemble=true
1\path=/your/path/data/trajectories/EUR_LL10/psfc_1000hPa_L62
1\ABLTrajectories=false
#1\schedulerID=SingleThread
1\schedulerID=MultiThread
1\memoryManagerID=Trajectories DF-T psfc_1000hPa_L62
